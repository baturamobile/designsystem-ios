# Empezar
Para empezar un nuevo proyecto valdría con copiar las siguientes carpetas en nuestro proyecto:
- Cell
- Resources
- Custom
- Extensions
- Style

# Colores

```swift
typealias Color = UIColor

extension Color {
    enum Emphasis {
        case high
        case medium
        case low
        case custom(value: CGFloat)

        var rawValue: CGFloat {
            switch self {
            case .high:
                return 1.0
            case .medium:
                return 0.74
            case .low:
                return 0.38
            case .custom(let value):
                return value
            }
        }
    }

    @nonobjc class var primary: UIColor {
        return UIColor(red: 138.0 / 255.0, green: 116.0 / 255.0, blue: 0.0, alpha: 1.0)
    }

    static func onPrimary(_ emphasis: Emphasis = .high) -> UIColor {
        return onPrimary.withAlphaComponent(emphasis.rawValue)
    }

    private class var onPrimary: UIColor {
        return UIColor.white
    }

    @nonobjc class var primaryDark: UIColor {
        return UIColor(red: 219.0 / 255.0, green: 165.0 / 255.0, blue: 8.0 / 255.0, alpha: 1.0)
    }

    @nonobjc class var primaryLight: UIColor {
        return UIColor(red: 253.0 / 255.0, green: 247.0 / 255.0, blue: 230.0 / 255.0, alpha: 1.0)
    }

    @nonobjc class var background: UIColor {
        return UIColor.white
    }

    static func onBackground(_ emphasis: Emphasis = .high) -> UIColor {
        return onBackground.withAlphaComponent(emphasis.rawValue)
    }

    private class var onBackground: UIColor {
        return UIColor(red: 28.0 / 255.0, green: 28.0 / 255.0, blue: 30 / 255.0, alpha: 1.0)
    }

    @nonobjc class var surface: UIColor {
        return UIColor(red: 250.0 / 255.0, green: 250.0 / 255.0, blue: 250.0 / 255.0, alpha: 1.0)
    }

    static func onSurface(_ emphasis: Emphasis = .high) -> UIColor {
        return onSurface.withAlphaComponent(emphasis.rawValue)
    }

    private class var onSurface: UIColor {
        return UIColor(red: 28.0 / 255.0, green: 28.0 / 255.0, blue: 30 / 255.0, alpha: 1.0)
    }

    @nonobjc class var error: UIColor {
        return UIColor(red: 230.0 / 255.0, green: 74.0 / 255.0, blue: 25.0 / 255.0, alpha: 1.0)
    }

    @nonobjc class var success: UIColor {
        return UIColor(red: 104.0 / 255.0, green: 159.0 / 255.0, blue: 56.0 / 255.0, alpha: 1.0)
    }

    @nonobjc class var alert: UIColor {
        return UIColor(red: 244.0 / 255.0, green: 183.0 / 255.0, blue: 9.0 / 255.0, alpha: 1.0)
    }

    @available(iOS, deprecated: 1.1, renamed: "onBackground()")
    @nonobjc class var base500: UIColor {
        return UIColor(white: 41.0 / 255.0, alpha: 1.0)
    }

    @available(iOS, deprecated: 1.1, renamed: "onBackground(emphasis:)")
    @nonobjc class var base400: UIColor {
        return UIColor(white: 117.0 / 255.0, alpha: 1.0)
    }

    @available(iOS, deprecated: 1.1, renamed: "onBackground(emphasis:)")
    @nonobjc class var base200: UIColor {
        return UIColor(white: 189.0 / 255.0, alpha: 1.0)
    }

    @available(iOS, deprecated: 1.1, renamed: "onBackground(emphasis:)")
    @nonobjc class var base100: UIColor {
        return UIColor(white: 250.0 / 255.0, alpha: 1.0)
    }

    @available(iOS, deprecated: 1.1, renamed: "background")
    @nonobjc class var baseWhite: UIColor {
        return UIColor(white: 1.0, alpha: 1.0)
    }
}
```
Para aplicar esta fuente a una UILabel abría que escribir el siguiente codigo:

```swift
  // UILabel con tamaño head1
  label.apply(Theme.Label.head1)

  // UILabel con tamaño body1Regular
  label.apply(Theme.Label.body1Regular)
```

# Botones

![Buttons](./images/buttons.png)

Para crear estos botones en los .xib debemos escribir en la Custom Class el botón correspondiente.

### TextButton
![TextButton](./images/text-button.png)

### ContainedButton
![ContainedButton](./images/contained-button.png)

### OutlineButton
![OutlineButton](./images/outline-button.png)


Para crearlos desde codigo:
```swift
let textButton = TextButton()
let outlineButton = OutlineButton()
let containedButton = ContainedButton()
```

# TopBars

### Title Center

![TitleCenter](./images/title-center.png)

```swift
title = "Center"
titleFont = Font.bold(size: .body1)
titleColor = Color.base1
preferLargeTitles = false
let item = UIBarButtonItem(image: #imageLiteral(resourceName: "circle"), style: .plain, target: nil, action: nil)
let item2 = UIBarButtonItem(image: #imageLiteral(resourceName: "circle"), style: .plain, target: nil, action: nil)
navigationItem.rightBarButtonItems = [item, item2]
```

### Title Large

![TitleLarge](./images/title-large.png)

```swift
title = "Large title"
titleFont = Font.bold(size: .body1)
largeTitleFont = Font.bold(size: .head1)
titleColor = Color.primary
preferLargeTitles = true

let item = UIBarButtonItem(image: #imageLiteral(resourceName: "circle"), style: .plain, target: nil, action: nil)
let item2 = UIBarButtonItem(title: "Label", style: .plain, target: nil, action: nil)
item2.tintColor = Color.base1
item2.font = Font.bold(size: .body2)
navigationItem.rightBarButtonItems = [item, item2]
```

# Bottom Bar

![BottomBar](./images/bottom-bar.png)

```swift
class BottomBarViewController: TabBarViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        tabBar.isTranslucent = false
        tabBar.tintColor = Color.primary

        let toolbarVC = ToolbarViewController()
        toolbarVC.tabBarItem = UITabBarItem(title: "Label", image: #imageLiteral(resourceName: "circle.pdf"), selectedImage: #imageLiteral(resourceName: "circle.pdf"))
        let toolbarVC1 = ToolbarViewController()
        toolbarVC1.tabBarItem = UITabBarItem(title: "Label", image: #imageLiteral(resourceName: "circle.pdf"), selectedImage: #imageLiteral(resourceName: "circle.pdf"))
        let toolbarVC2 = ToolbarViewController()
        toolbarVC2.tabBarItem = UITabBarItem(title: "Label", image: #imageLiteral(resourceName: "circle.pdf"), selectedImage: #imageLiteral(resourceName: "circle.pdf"))
        let toolbarVC3 = ToolbarViewController()
        toolbarVC3.tabBarItem = UITabBarItem(title: "Label", image: #imageLiteral(resourceName: "circle.pdf"), selectedImage: #imageLiteral(resourceName: "circle.pdf"))
        setViewControllers([toolbarVC, toolbarVC1, toolbarVC2, toolbarVC3], animated: true)

        badge(value: "1", tabIndex: 0)
        badge(value: "Test", tabIndex: 1)
        badge(value: " ", tabIndex: 2)
    }
}
```
# Forms

![Forms](./images/forms.png)

![Forms](./images/forms1.png)
```swift
class FormsViewController: UIViewController {

    @IBOutlet weak var textInputBordered: TextInput!
    @IBOutlet weak var textInputBorderedAssistive: TextInput!
    @IBOutlet weak var textInputFilled: TextInput!
    @IBOutlet weak var textInputFilledAssistive: TextInput!

    override func viewDidLoad() {
        super.viewDidLoad()
        textInputBordered.delegate = self
        textInputFilled.delegate = self
        textInputBorderedAssistive.delegate = self
        textInputFilledAssistive.delegate = self

        textInputFilled.isBordered = false
        textInputFilledAssistive.isBordered = false

        textInputBorderedAssistive.assistiveText = "AssistiveText"
        textInputFilledAssistive.assistiveText = "AssistiveText"

        textInputFilledAssistive.imageLeft =  #imageLiteral(resourceName: "point.pdf")

        textInputBordered.validationCompletion = validateFunction
        textInputBordered.errorText = "Escribe Prueba"

        textInputBorderedAssistive.text = "Hola"
        textInputBorderedAssistive.isSecureText = true
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }

    func validateFunction() -> Bool {
        return textInputBordered.text == "Prueba"
    }
}

extension FormsViewController: TextInputDelegate {
    func textInputShouldReturn(_ textField: UITextField) -> Bool {
        return textField.endEditing(true)
    }
}
```

# Dialogs
### Dialog1
![Dialog1](./images/dialog1.png)
```swift
let alert = AlertViewController()
alert.show(image: #imageLiteral(resourceName: "11.jpg"),
           title: "Headline 6",
           info: "Apparently we had reached a great height in the atmosphere, for the...",
           titleOk: "OK",
           titleCancel: "Cancel")
```

### Dialog2
![Dialog2](./images/dialog2.png)
```swift
let alert = AlertViewController()
alert.show(title: "Headline 6",
           info: "Apparently we had reached a great height in the atmosphere, for the...",
           titleOk: "OK",
           titleCancel: "Cancel",
           completionOk: nil,
           completionCancel: nil)
```

### Dialog3
![Dialog3](./images/dialog3.png)
```swift
let alert = AlertViewController()
alert.show(info: "Apparently we had reached a great height in the atmosphere, for the...",
           titleOk: "OK",
           titleCancel: "Cancel",
           completionOk: nil,
           completionCancel: nil)
```
# SearchBar
![SearchBar](./images/searchbar.png)

```swift
class SearchBarViewController: ViewController {

    @IBOutlet weak var tableView: UITableView!

    var searchController: UISearchController?

    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Search"
        searchController = UISearchController(searchResultsController: nil)
        if #available(iOS 11.0, *) {
            navigationItem.searchController = searchController
        } else {
            tableView.tableHeaderView = searchController?.searchBar
        }
        definesPresentationContext = true
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        searchController?.searchBar.apply(Theme.SearchBarStyle.searchBar)
    }
}
```

# Snackbar

![SnackbarAction](./images/snackbar.png)

```swift
let snackbar = Snackbar()
view.addSubview(snackbar)
snackbar.text = "Solo texto Solo texto Solo texto Solo texto Solo texto Solo texto"
snackbar.action = nil
snackbar.show(hideAfter: 3)
```

![Snackbar](./images/snackbar-action.png)
```swift
let snackbar = Snackbar()
view.addSubview(snackbar)
snackbar.text = "Con boton Con boton Con boton Con boton Con boton Con boton Con boton"
snackbar.action = SnackbarAction(title: "ACTION") {
    print("Hola")
}

snackbar.show()
```

# Cells
![Cells](./images/list.jpeg)

Dentro de Zero, copiar el Stack View de la celda y llevarla a nuestro proyecto.

![List Xib](images/list-xcode.png)

Copiar la clase BaturaTableViewCell, sustituir typealias Object por el modelo del objeto que vayamos a pintar.

En la función configureWith(item:) ocultar, mostrar y rellenar los campos que sean necesarios.

```swift
class BaturaTableViewCell: UITableViewCell, TableViewCell {
    typealias Object = Any

    var item: Any?
    weak var delegate: TableViewCellDelegate?

    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelDesc: UILabel!
    @IBOutlet weak var labelInfo: UILabel!
    @IBOutlet weak var imageViewLeft: UIImageView!
    @IBOutlet weak var viewImageLeft: UIView!
    @IBOutlet weak var buttonRight: ZeroTextButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        setupStyle()
    }

    override func prepareForReuse() {
        super.prepareForReuse()
    }

    func setupStyle() {
        labelDesc.apply(Theme.LabelFonts.caption1Light)
        labelInfo.apply(Theme.LabelFonts.caption2Regular)
        labelTitle.apply(Theme.LabelFonts.body1)
        labelInfo.textColor = Color.onBackground(.medium)
        buttonRight.style = .normal
    }

    func configureWith(item: Any?) {

    }

    @IBAction func didTapActionButton(_ sender: Any) {
        delegate?.didTapActionButton(item: item)
    }
}
```

En el dataSource del tableView utilizar:

```swift
 func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: BaturaTableViewCell.reuseID(),
                                                 for: indexPath) as? BaturaTableViewCell
        cell?.configureWith(item: items[indexPath.row])
        cell?.delegate = self
        return cell!
    }
```

# Chips

![Chips](images/chips.jpeg)

```swift
class ChipsViewController: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!

    override func viewDidLoad() {
        super.viewDidLoad()
        setupCollectionView()
    }

    func setupCollectionView() {
        let layout = MDCChipCollectionViewFlowLayout()
        let cell = MDCChipCollectionViewCell()
        if #available(iOS 10.0, *) {
            layout.estimatedItemSize = UICollectionViewFlowLayout.automaticSize
        } else {
            layout.estimatedItemSize = cell.intrinsicContentSize
        }
        collectionView.collectionViewLayout = layout
        collectionView.allowsMultipleSelection = true
        collectionView.register(MDCChipCollectionViewCell.self, forCellWithReuseIdentifier: "identifier")
        collectionView.dataSource = self
    }

    func randomString(length: Int) -> String {
      let letters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
      return String((0..<length).map { _ in letters.randomElement()! })
    }
}

extension ChipsViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 10
    }

    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "identifier",
                                                      for: indexPath) as? MDCChipCollectionViewCell
        cell?.alwaysAnimateResize = false
        let chipView = cell?.chipView
        chipView?.titleLabel.text = randomString(length: indexPath.row + 2)
        chipView?.imageView.image = #imageLiteral(resourceName: "bike.pdf").withRenderingMode(.alwaysTemplate)
        chipView?.selectedImageView.image = #imageLiteral(resourceName: "bike.pdf").withRenderingMode(.alwaysTemplate)
        chipView?.selectedImageView.tintColor = .primary
        return cell!
    }
}

```

# Banner
![Banner](images/banner.jpeg)

```swift
let banner = Banner()
banner.text = "Lorem ipsum dolor sit amet consectetur adipiscing elit etiam"
banner.buttonOkText = "Ok"
banner.buttonCancelText = "Cancel"
banner.image = #imageLiteral(resourceName: "avatar40.png")
banner.delegate = self
view.addSubview(banner)

//Delegate methods
@objc protocol BannerDelegate: class {
    @objc optional func bannerWillAppear(banner: Banner)
    @objc optional func bannerDidAppear(banner: Banner)
    @objc optional func bannerWillDismiss(banner: Banner)
    @objc optional func bannerDidDismiss(banner: Banner)
}
```
