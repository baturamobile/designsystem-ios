//
//  AppDelegate.swift
//  designsystem
//
//  Created by Rubén Alonso on 25/02/2019.
//  Copyright © 2019 Batura. All rights reserved.
//

import UIKit
import Zero
import Firebase

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication,
                     didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {

        FirebaseApp.configure()
        ZeroTheme.apply()

        let frame = UIScreen.main.bounds
        window = UIWindow(frame: frame)

        let menuVC = MenuViewController()
        let nav = UINavigationController(rootViewController: menuVC)
        window?.rootViewController = nav
        window?.makeKeyAndVisible()

        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
    }

    func applicationWillTerminate(_ application: UIApplication) {

    }

}
