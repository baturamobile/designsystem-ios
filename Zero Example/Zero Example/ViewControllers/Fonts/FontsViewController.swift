//
//  FontsViewController.swift
//  designsystem
//
//  Created by Rubén Alonso on 25/02/2019.
//  Copyright © 2019 Batura. All rights reserved.
//

import UIKit
import Zero

class FontsViewController: UIViewController {

    @IBOutlet weak var lblTitle1: UILabel!
    @IBOutlet weak var lblTitle1Title: UILabel!
    @IBOutlet weak var lblTitle2: UILabel!
    @IBOutlet weak var lblTitle2Title: UILabel!
    @IBOutlet weak var lblTitle3: UILabel!
    @IBOutlet weak var lblTitle3Title: UILabel!
    @IBOutlet weak var lblTitle4: UILabel!
    @IBOutlet weak var lblTitle4Title: UILabel!
    @IBOutlet weak var lblTitle5: UILabel!
    @IBOutlet weak var lblTitle5Title: UILabel!

    @IBOutlet weak var lblBody1: UILabel!
    @IBOutlet weak var lblBody1Title: UILabel!
    @IBOutlet weak var lblBody2: UILabel!
    @IBOutlet weak var lblBody2Title: UILabel!
    @IBOutlet weak var lblBody3: UILabel!
    @IBOutlet weak var lblBody3Title: UILabel!
    @IBOutlet weak var lblBody4: UILabel!
    @IBOutlet weak var lblBody4Title: UILabel!
    @IBOutlet weak var lblBody5: UILabel!
    @IBOutlet weak var lblBody5Title: UILabel!
    @IBOutlet weak var lblBody6: UILabel!
    @IBOutlet weak var lblBody6Title: UILabel!

    @IBOutlet weak var lblCaption1: UILabel!
    @IBOutlet weak var lblCaption1Title: UILabel!
    @IBOutlet weak var lblCaption2: UILabel!
    @IBOutlet weak var lblCaption2Title: UILabel!
    @IBOutlet weak var lblCaption3: UILabel!
    @IBOutlet weak var lblCaption3Title: UILabel!
    @IBOutlet weak var lblCaption4: UILabel!
    @IBOutlet weak var lblCaption4Title: UILabel!

    @IBOutlet weak var btn1: UIButton!
    @IBOutlet weak var btn2: UIButton!
    @IBOutlet weak var btn3: UIButton!

    let textExample = "Lorem ipsum dolor sit amet, consectetur adipiscing elit."

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupTexts()
        setupStyle()
    }

    func setupTexts() {
        lblTitle1Title.text = "Headline 34 Bold"
        lblTitle2Title.text = "Headline 28 Bold"
        lblTitle3Title.text = "Headline 22 Bold"
        lblTitle4Title.text = "Headline 20 Bold"
        lblTitle5Title.text = "Headline 20 Medium"
        lblBody1Title.text = "Body 17 Bold"
        lblBody2Title.text = "Body 17 Medium"
        lblBody3Title.text = "Body 17 Light"
        lblBody4Title.text = "Body 15 Bold"
        lblBody5Title.text = "Body 15 Medium"
        lblBody6Title.text = "Body 15 Light"
        lblCaption1Title.text = "Caption 13 Bold"
        lblCaption2Title.text = "Caption 13 Medium"
        lblCaption3Title.text = "Caption 11 Bold"
        lblCaption4Title.text = "Caption 11 Medium"
        lblTitle1.text = textExample
        lblTitle2.text = textExample
        lblTitle3.text = textExample
        lblTitle4.text = textExample
        lblTitle5.text = textExample
        lblBody1.text = textExample
        lblBody2.text = textExample
        lblBody3.text = textExample
        lblBody4.text = textExample
        lblBody5.text = textExample
        lblBody6.text = textExample
        lblCaption1.text = textExample
        lblCaption2.text = textExample
        lblCaption3.text = textExample
        lblCaption4.text = textExample
    }

    func setupStyle() {
        ZeroTheme.LabelFonts.caption2.apply(to:
            [lblTitle1Title, lblTitle2Title, lblTitle3Title, lblTitle4Title, lblTitle5Title,
            lblBody1Title, lblBody2Title, lblBody3Title, lblBody4Title, lblBody5Title, lblBody6Title,
            lblCaption1Title, lblCaption2Title, lblCaption3Title, lblCaption4Title])

        lblTitle1Title.textColor = ZeroColor.primary
        lblTitle2Title.textColor = ZeroColor.primary
        lblTitle3Title.textColor = ZeroColor.primary
        lblTitle4Title.textColor = ZeroColor.primary
        lblTitle5Title.textColor = ZeroColor.primary

        lblBody1Title.textColor = ZeroColor.primary
        lblBody2Title.textColor = ZeroColor.primary
        lblBody3Title.textColor = ZeroColor.primary
        lblBody4Title.textColor = ZeroColor.primary
        lblBody5Title.textColor = ZeroColor.primary
        lblBody6Title.textColor = ZeroColor.primary

        lblCaption1Title.textColor = ZeroColor.primary
        lblCaption2Title.textColor = ZeroColor.primary
        lblCaption3Title.textColor = ZeroColor.primary
        lblCaption4Title.textColor = ZeroColor.primary

        lblTitle1.apply(ZeroTheme.LabelFonts.head1)
        lblTitle2.apply(ZeroTheme.LabelFonts.head2)
        lblTitle3.apply(ZeroTheme.LabelFonts.head3)
        lblTitle4.apply(ZeroTheme.LabelFonts.head4)
        lblTitle5.apply(ZeroTheme.LabelFonts.head4Regular)

        lblBody1.apply(ZeroTheme.LabelFonts.body1)
        lblBody2.apply(ZeroTheme.LabelFonts.body1Regular)
        lblBody3.apply(ZeroTheme.LabelFonts.body1Light)

        lblBody4.apply(ZeroTheme.LabelFonts.body2)
        lblBody5.apply(ZeroTheme.LabelFonts.body2Regular)
        lblBody6.apply(ZeroTheme.LabelFonts.body2Light)

        lblCaption1.apply(ZeroTheme.LabelFonts.caption1)
        lblCaption2.apply(ZeroTheme.LabelFonts.caption1Regular)

        lblCaption3.apply(ZeroTheme.LabelFonts.caption2)
        lblCaption4.apply(ZeroTheme.LabelFonts.caption2Regular)
    }
}
