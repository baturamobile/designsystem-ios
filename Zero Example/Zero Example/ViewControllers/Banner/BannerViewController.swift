//
//  BannerViewController.swift
//  designsystem
//
//  Created by Rubén Alonso on 17/02/2020.
//  Copyright © 2020 Batura. All rights reserved.
//

import UIKit
import Zero

class BannerViewController: UIViewController {

    @IBOutlet weak var scrollView: UIScrollView!
    var banner = ZeroBanner()

    override func viewDidLoad() {
        super.viewDidLoad()

        setupBanner()
    }

    func setupBanner() {
        banner.text = "Lorem ipsum dolor sit amet consectetur adipiscing elit etiam"
        banner.buttonOkText = "Ok"
        banner.buttonCancelText = "Cancel"
        banner.image = #imageLiteral(resourceName: "avatar40.png")
        banner.delegate = self
        view.addSubview(banner)
    }

    @IBAction func showBanner(_ sender: Any) {
        if banner.isHidden {
            banner.show()
        }
    }
}

extension BannerViewController: ZeroBannerDelegate {
    func bannerDidAppear(banner: ZeroBanner) {
        scrollView.contentInset = UIEdgeInsets(top: banner.bounds.height,
                                               left: 0,
                                               bottom: 0,
                                               right: 0)

        scrollView.contentOffset.y -= banner.bounds.height

    }
    func bannerWillDismiss(banner: ZeroBanner) {
        scrollView.contentInset = UIEdgeInsets(top: 0,
                                               left: 0,
                                               bottom: 0,
                                               right: 0)
    }
}
