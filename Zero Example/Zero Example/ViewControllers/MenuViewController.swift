//
//  MenuViewController.swift
//  designsystem
//
//  Created by Rubén Alonso on 01/03/2019.
//  Copyright © 2019 Batura. All rights reserved.
//

import UIKit
import Zero

class MenuViewController: ZeroViewController {

    @IBOutlet weak var tableView: UITableView!

    let items : [(icon: UIImage?, text: String, vc: UIViewController.Type)] = [
        (#imageLiteral(resourceName: "iconOverviewType.pdf"), "Type", FontsViewController.self),
        (#imageLiteral(resourceName: "iconOverviewColor"), "Color", ColorsViewController.self),
        (#imageLiteral(resourceName: "iconOverviewButton"), "Button", ButtonsViewController.self),
        (#imageLiteral(resourceName: "iconOverviewTopBar"), "TopBar", TopBarViewController.self),
        (#imageLiteral(resourceName: "iconOverviewBottomBar"), "BottomBar", BottomBarViewController.self),
        (#imageLiteral(resourceName: "iconOverviewSearchBar"), "SearchBar", SearchBarViewController.self),
        (#imageLiteral(resourceName: "iconOverviewSnackbar.pdf"), "Sanckbar", SnackbarViewController.self),
        (#imageLiteral(resourceName: "iconOverviewForm.pdf"), "Textfield", FormsViewController.self),
        (#imageLiteral(resourceName: "iconOverviewDialog"), "Dialog", DialogsViewController.self),
        (#imageLiteral(resourceName: "iconOverviewList"), "List", ListsViewController.self),
        (#imageLiteral(resourceName: "iconOverviewBanner"), "Banner", BannerViewController.self),
        (#imageLiteral(resourceName: "iconOverviewChip"), "Chip", ChipsViewController.self),
        (#imageLiteral(resourceName: "iconOverviewSheets.pdf"), "Bottom Sheets", ActionSheetViewController.self),
        (#imageLiteral(resourceName: "iconOverviewSelection.pdf"), "Selection Control", ControlsViewController.self),
        (#imageLiteral(resourceName: "iconOverviewEmpty.pdf"), "Empty State", EmptyViewController.self)
    ]

    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }

    func setupTableView() {
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "CellID")
        tableView.dataSource = self
        tableView.delegate = self
        tableView.tableFooterView = UIView()
    }
}

// MARK: - UITableViewDataSource
extension MenuViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellID",
                                                 for: indexPath)
        cell.selectionStyle = .none
        cell.textLabel?.text = items[indexPath.row].text
        cell.textLabel?.font = .regular(size: .body1)
        cell.imageView?.image = items[indexPath.row].icon
        return cell
    }
}

extension MenuViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let item = items[indexPath.row]
        navigationController?.pushViewController(item.vc.init(), animated: true)
    }
}
