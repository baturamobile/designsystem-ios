//
//  SnackbarViewController.swift
//  designsystem
//
//  Created by Rubén Alonso on 25/04/2019.
//  Copyright © 2019 Batura. All rights reserved.
//

import UIKit
import Zero

class SnackbarViewController: UIViewController {

    @IBOutlet weak var btnText: ZeroTextButton!
    @IBOutlet weak var btnButton: ZeroTextButton!

    var snackbar: ZeroSnackbar!

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        snackbar = ZeroSnackbar()
        view.addSubview(snackbar)
    }

    @IBAction func didTapShowText() {
        snackbar.text = "Solo texto Solo texto Solo texto Solo texto Solo texto Solo texto"
        snackbar.action = nil
        snackbar.show(hideAfter: 3)
    }

    @IBAction func didTapShowButton() {
        snackbar.text = "Con boton Con boton Con boton Con boton Con boton Con boton Con boton"
        snackbar.action = ZeroSnackbarAction(title: "ACTION") {
            print("Hola")
        }

        snackbar.show()
    }
}
