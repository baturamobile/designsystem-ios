//
//  EmptyViewController.swift
//  designsystem
//
//  Created by Rubén Alonso on 11/06/2020.
//  Copyright © 2020 Batura. All rights reserved.
//

import UIKit
import Zero

class EmptyViewController: ZeroViewController {

    let empty = ZeroEmpty()

    override func viewDidLoad() {
        super.viewDidLoad()

        empty.image = #imageLiteral(resourceName: "empty.pdf")
        empty.title = "No hay datos que mostrar"
        empty.desc = "Crea una cuenta para poder acceder a este apartado. Gracias."
        empty.buttonText = "Crear cuenta"
        empty.buttonAction = {
            print("Hola")
        }
        empty.show(inView: self.view)
    }
}
