//
//  ListCell.swift
//  designsystem
//
//  Created by Rubén Alonso on 13/03/2020.
//  Copyright © 2020 Batura. All rights reserved.
//

import UIKit
import Zero

class ListCell: UITableViewCell, TableViewCell {
    typealias Object = Item
    var item: Item?
    weak var delegate: TableViewCellDelegate?

    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelDesc: UILabel!
    @IBOutlet weak var labelInfo: UILabel!
    @IBOutlet weak var imageViewLeft: UIImageView!
    @IBOutlet weak var buttonRight: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        tintColor = ZeroColor.primary
        setupStyle()
    }

    override func prepareForReuse() {
        super.prepareForReuse()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    func setupStyle() {
        labelDesc.apply(ZeroTheme.LabelFonts.body2Light)
        labelInfo.apply(ZeroTheme.LabelFonts.caption2Regular)
        labelInfo.textColor = ZeroColor.onBackground(.medium)
        labelDesc.textColor = ZeroColor.onBackground(.medium)
        labelTitle.apply(ZeroTheme.LabelFonts.body2)
    }

    func configureWith(item: Item?) {
        labelTitle.text = item?.title
        labelTitle.isHidden = item?.title?.isEmpty ?? true
        labelInfo.text = item?.info
        labelInfo.isHidden = item?.info?.isEmpty ?? true
        labelDesc.text = item?.desc
        labelDesc.isHidden = item?.desc?.isEmpty ?? true
        imageViewLeft.image = item?.image
        imageViewLeft.isHidden = item?.image == nil
        buttonRight.isHidden = item?.icon == nil
        buttonRight.setImage(item?.icon, for: .normal)
    }

    @IBAction func didTapActionButton(_ sender: Any) {
        delegate?.didTapActionButton(item: item)
    }
}
