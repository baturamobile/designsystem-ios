//
//  ListsViewController.swift
//  designsystem
//
//  Created by Rubén Alonso on 02/07/2019.
//  Copyright © 2019 Batura. All rights reserved.
//

import UIKit
import Zero

struct Item {
    var image: UIImage?
    var title: String?
    var desc: String?
    var info: String?
    var icon: UIImage?
}

class ListsViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!

    var items: [Item] = [
        Item(image: nil,
             title: "One line list",
             info: "Label"),
        Item(image: #imageLiteral(resourceName: "default.pdf"),
             title: "One line list",
             icon: #imageLiteral(resourceName: "action.pdf")),
        Item(image: #imageLiteral(resourceName: "avatar40"),
             title: "One line list",
             desc: nil,
             icon: #imageLiteral(resourceName: "action.pdf")),
        Item(image: nil,
             title: "Two-line list",
             desc: "Caption line",
             info: "Label"),
        Item(image: nil,
             title: "Two-line list",
             desc: "Caption line",
             icon: #imageLiteral(resourceName: "action.pdf")),
        Item(image: #imageLiteral(resourceName: "default.pdf"),
             title: "Two line list",
             desc: "Caption line",
             icon: nil),
        Item(image: #imageLiteral(resourceName: "avatar40"),
             title: "Two line list",
             desc: "Multiple line Multiple line",
             icon: nil),
        Item(image: nil,
             title: "Three line list",
             desc: "Multiple line Multiple line Multiple line Multiple line Multiple line",
             info: "Label"),
        Item(image: nil,
             title: "Two line list",
             desc: "Multiple line Multiple line Multiple",
             icon: #imageLiteral(resourceName: "action.pdf")),
        Item(image: #imageLiteral(resourceName: "avatar40"),
             title: "Two line list",
             desc: "Multiple line Multiple line Multiple line Multiple line Multiple line"),
        Item(image: #imageLiteral(resourceName: "imageList.png"),
             title: "Two line list",
             desc: "Multiple line Multiple line Multiple line Multiple line Multiple line")
    ]

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        setupTableView()
    }

    func setupTableView() {
        tableView.register(ListCell.nib(),
                           forCellReuseIdentifier: ListCell.reuseID())
        tableView.rowHeight = UITableView.automaticDimension
        tableView.tableFooterView = UIView()
        tableView.dataSource = self
    }
}

extension ListsViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ListCell.reuseID(),
                                                 for: indexPath) as? ListCell
        cell?.configureWith(item: items[indexPath.row])
        cell?.delegate = self
        return cell!
    }
}

extension ListsViewController: TableViewCellDelegate {
    func didTapActionButton(item: Any?) {
        guard let item = item as? Item else {
            return
        }
        print(item)
    }
}
