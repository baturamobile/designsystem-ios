//
//  ColorsViewController.swift
//  designsystem
//
//  Created by Rubén Alonso on 26/03/2019.
//  Copyright © 2019 Batura. All rights reserved.
//

import UIKit
import Zero

class ColorsViewController: UIViewController {

    @IBOutlet weak var viewOnPrimaryHight: UIView!
    @IBOutlet weak var viewOnPrimaryMedium: UIView!
    @IBOutlet weak var viewOnPrimaryLow: UIView!
    @IBOutlet weak var viewPrimary: UIView!
    @IBOutlet weak var viewPrimaryDark: UIView!
    @IBOutlet weak var viewPrimaryLight: UIView!

    @IBOutlet weak var viewBase01: UIView!
    @IBOutlet weak var viewBase02: UIView!
    @IBOutlet weak var viewBase03: UIView!
    @IBOutlet weak var viewBase05: UIView!

    @IBOutlet weak var viewSurface: UIView!
    @IBOutlet weak var viewOnSurfaceHight: UIView!
    @IBOutlet weak var viewOnSurfaceMedium: UIView!
    @IBOutlet weak var viewOnSurfaceLow: UIView!

    @IBOutlet weak var viewError: UIView!
    @IBOutlet weak var viewAlert: UIView!
    @IBOutlet weak var viewSuccess: UIView!

    override func viewDidLoad() {
        super.viewDidLoad()
        setupStyle()
    }

    func setupStyle() {
        UILabel.appearance(whenContainedInInstancesOf: [Self.self]).font = ZeroFont.regular(size: .caption1)
        viewPrimary.backgroundColor = ZeroColor.primary

        viewBase01.backgroundColor = ZeroColor.onBackground()
        viewBase02.backgroundColor = ZeroColor.onBackground(.medium)
        viewBase03.backgroundColor = ZeroColor.onBackground(.low)
        viewBase05.backgroundColor = ZeroColor.background

        viewOnSurfaceHight.backgroundColor = ZeroColor.onSurface()
        viewOnSurfaceMedium.backgroundColor = ZeroColor.onSurface(.medium)
        viewOnSurfaceLow.backgroundColor = ZeroColor.onSurface(.low)
        viewSurface.backgroundColor = ZeroColor.surface

        let colors = [
            ZeroColor.onPrimary(),
            ZeroColor.onPrimary(.medium),
            ZeroColor.onPrimary(.low)
        ]

        for (index, color) in colors.enumerated() {
            let layerBase = CALayer()
            layerBase.backgroundColor = ZeroColor.primary.cgColor
            let layerOn = CALayer()
            layerOn.backgroundColor = color.cgColor
            switch index {
            case 0:
                layerBase.frame = viewOnPrimaryHight.bounds
                layerOn.frame = viewOnPrimaryHight.bounds
                viewOnPrimaryHight.layer.addSublayer(layerBase)
                viewOnPrimaryHight.layer.addSublayer(layerOn)
            case 1:
                layerBase.frame = viewOnPrimaryMedium.bounds
                layerOn.frame = viewOnPrimaryMedium.bounds
                viewOnPrimaryMedium.layer.addSublayer(layerBase)
                viewOnPrimaryMedium.layer.addSublayer(layerOn)
            case 2:
                layerBase.frame = viewOnPrimaryLow.bounds
                layerOn.frame = viewOnPrimaryLow.bounds
                viewOnPrimaryLow.layer.addSublayer(layerBase)
                viewOnPrimaryLow.layer.addSublayer(layerOn)
            default:
                viewOnPrimaryHight.layer.addSublayer(layerBase)
                viewOnPrimaryHight.layer.addSublayer(layerOn)
            }
        }

        viewError.backgroundColor = ZeroColor.error
        viewAlert.backgroundColor = ZeroColor.alert
        viewSuccess.backgroundColor = ZeroColor.success
    }
}
