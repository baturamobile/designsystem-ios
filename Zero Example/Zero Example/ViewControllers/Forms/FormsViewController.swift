//
//  FormsViewController.swift
//  designsystem
//
//  Created by Rubén Alonso on 06/05/2019.
//  Copyright © 2019 Batura. All rights reserved.
//

import UIKit
import Zero
import MaterialComponents

class FormsViewController: UIViewController {

    @IBOutlet weak var inputOutlined: ZeroTextField!
    @IBOutlet weak var inputOutlinedError: ZeroTextField!
    @IBOutlet weak var inputFilled: ZeroTextField!
    @IBOutlet weak var inputFilledHelper: ZeroTextField!

    var inputControllerOutlined: ZeroTextFieldControllerFilled?
    var inputControllerFilled: ZeroTextFieldControllerFilled?
    var inputControllerOutlinedError: ZeroTextFieldControllerFilled?
    var inputControllerFilledHelper: ZeroTextFieldControllerFilled?

    override func viewDidLoad() {
        super.viewDidLoad()

        inputControllerOutlined = ZeroTextFieldControllerFilled(textInput: inputOutlined)
        inputControllerOutlined?.placeholderText = "Prueba"
        inputOutlined.isSecureTextEntry = true
        inputOutlined.delegate = self

        inputControllerOutlinedError = ZeroTextFieldControllerFilled(textInput: inputOutlinedError)
        inputControllerOutlinedError?.placeholderText = "Prueba"
        inputControllerOutlinedError?.validationCompletion = checkEmail
        inputOutlinedError.delegate = self

        inputControllerFilled = ZeroTextFieldControllerFilled(textInput: inputFilled)
        inputControllerFilled?.placeholderText = "Prueba"
        inputFilled.delegate = self

        inputControllerFilledHelper = ZeroTextFieldControllerFilled(textInput: inputFilledHelper)
        inputControllerFilledHelper?.placeholderText = "Prueba"
        inputControllerFilledHelper?.helperText = "Texto de ayuda"
        inputFilledHelper.delegate = self
    }

    func checkEmail() -> Bool {
        let valid = inputOutlinedError.text?.validate(type: .nif) ?? false
        if !valid {
            inputControllerOutlinedError?.setErrorText("NIF MALO",
                                                       errorAccessibilityValue: "NIF MALO")
        } else {
            inputControllerOutlinedError?.setErrorText(nil,
                                                       errorAccessibilityValue: nil)
        }
        return valid
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
}

extension FormsViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return textField.endEditing(true)
    }

    func textFieldDidEndEditing(_ textField: UITextField) {
        switch textField {
        case inputOutlinedError:
            inputControllerOutlinedError?.validate()
        default:
            break
        }
    }
}
