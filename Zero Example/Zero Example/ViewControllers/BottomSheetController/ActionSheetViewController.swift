//
//  ActionSheetViewController.swift
//  designsystem
//
//  Created by Rubén Alonso on 04/03/2020.
//  Copyright © 2020 Batura. All rights reserved.
//

import UIKit
import Zero

class ActionSheetViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    @IBAction func openBottomSheet(_ sender: Any) {
        let bottomSheet = ZeroBottomSheet(title: "Title option")
        let action = ZeroBottomSheetAction(title: "Make a copy",
                                       image: #imageLiteral(resourceName: "circle.pdf"),
                                       style: .default) {
                                        print("Make a copy")
        }
        let action2 = ZeroBottomSheetAction(title: "Get link",
                                       image: #imageLiteral(resourceName: "circle.pdf"),
                                       style: .default) {
                                        print("Get link")
        }
        let cancel = ZeroBottomSheetAction(title: "Edit name",
                                       image: #imageLiteral(resourceName: "circle.pdf"),
                                       style: .default)

        bottomSheet.add(action: action)
        bottomSheet.add(action: action2)
        bottomSheet.add(action: cancel)

        present(bottomSheet, animated: false, completion: nil)
    }
}
