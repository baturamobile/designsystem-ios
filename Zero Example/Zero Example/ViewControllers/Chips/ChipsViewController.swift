//
//  ChipsViewController.swift
//  designsystem
//
//  Created by Rubén Alonso on 17/02/2020.
//  Copyright © 2020 Batura. All rights reserved.
//

import UIKit
import Zero
import MaterialComponents.MaterialChips

class ChipsViewController: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!

    override func viewDidLoad() {
        super.viewDidLoad()
        setupCollectionView()
    }

    func setupCollectionView() {
        let layout = MDCChipCollectionViewFlowLayout()
        let cell = MDCChipCollectionViewCell()
        if #available(iOS 10.0, *) {
            layout.estimatedItemSize = UICollectionViewFlowLayout.automaticSize
        } else {
            layout.estimatedItemSize = cell.intrinsicContentSize
        }
        collectionView.collectionViewLayout = layout
        collectionView.allowsMultipleSelection = true
        collectionView.register(MDCChipCollectionViewCell.self, forCellWithReuseIdentifier: "identifier")
        collectionView.dataSource = self
    }

    func randomString(length: Int) -> String {
      let letters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
      return String((0..<length).map { _ in letters.randomElement()! })
    }
}

extension ChipsViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 10
    }

    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "identifier",
                                                      for: indexPath) as? MDCChipCollectionViewCell
        cell?.alwaysAnimateResize = false
        let chipView = cell?.chipView
        chipView?.titleLabel.text = randomString(length: indexPath.row + 2)
        chipView?.imageView.image = #imageLiteral(resourceName: "bike.pdf").withRenderingMode(.alwaysTemplate)
        chipView?.selectedImageView.image = #imageLiteral(resourceName: "bike.pdf").withRenderingMode(.alwaysTemplate)
        chipView?.selectedImageView.tintColor = .primary
        return cell!
    }
}
