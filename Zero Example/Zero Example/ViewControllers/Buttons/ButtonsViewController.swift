//
//  ButtonsViewController.swift
//  designsystem
//
//  Created by Rubén Alonso on 26/03/2019.
//  Copyright © 2019 Batura. All rights reserved.
//

import UIKit
import Zero

class ButtonsViewController: UIViewController {
    @IBOutlet weak var btnContainedEnabled: ZeroContainedButton!
    @IBOutlet weak var btnContainedDisabled: ZeroContainedButton!

    @IBOutlet weak var btnOutlineEnabled: ZeroOutlineButton!
    @IBOutlet weak var btnOutlineDisabled: ZeroOutlineButton!

    @IBOutlet weak var btnError: ZeroTextButton!
    @IBOutlet weak var btnAlert: ZeroTextButton!
    @IBOutlet weak var btnSuccess: ZeroTextButton!
    @IBOutlet weak var btnNormal: ZeroTextButton!
    @IBOutlet weak var btnRightImage: ButtonRightImage!

    override func viewDidLoad() {
        super.viewDidLoad()
        setupStyle()
    }

    func setupStyle() {
        btnNormal.style = .normal
        btnError.style = .error
        btnAlert.style = .alert
        btnSuccess.style = .success
        btnSuccess.setTitle("Prueba", for: .normal)
        btnContainedEnabled.completion {
            self.prueba {
                self.prueba2()
            }
        }
    }

    func prueba(_ completion:() -> Void ) {
        completion()
    }

    func prueba2() {
        print("Hola")
    }

    @IBAction func printText(_ sender: Any) {
        print("Right Button")
    }
}
