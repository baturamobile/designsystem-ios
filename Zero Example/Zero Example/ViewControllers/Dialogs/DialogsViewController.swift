//
//  DialogsViewController.swift
//  designsystem
//
//  Created by Rubén Alonso on 11/06/2019.
//  Copyright © 2019 Batura. All rights reserved.
//

import UIKit
import Zero

class DialogsViewController: UIViewController {

    @IBOutlet weak var buttonDialog: ZeroTextButton!
    @IBOutlet weak var buttonDialogWithoutImage: ZeroTextButton!
    @IBOutlet weak var buttonDialogWithoutTitle: ZeroTextButton!
    @IBOutlet weak var buttonDialogWithoutDesc: ZeroTextButton!

    var alert = ZeroDialog()
    var banner = ZeroBanner()

    override func viewDidLoad() {
        super.viewDidLoad()

        buttonDialog.apply(ZeroTheme.Button.normal)
        buttonDialogWithoutImage.apply(ZeroTheme.Button.normal)
        buttonDialogWithoutTitle.apply(ZeroTheme.Button.normal)
        buttonDialogWithoutDesc.apply(ZeroTheme.Button.normal)

        buttonDialog.completion(didTapDialog)
        buttonDialogWithoutImage.completion(didTapDialogWithoutImage)
        buttonDialogWithoutTitle.completion(didTapDialogWithoutTitle)
        buttonDialogWithoutDesc.completion(didTapDialogWithoutDesc)
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }

    func didTapDialog() {
        alert.show(image: #imageLiteral(resourceName: "11.jpg"),
                   title: "Headline 6",
                   info: "Apparently we had reached a great height in the atmosphere, for the...",
                   titleOk: "OK",
                   titleCancel: "Cancel")
    }

    func didTapDialogWithoutImage() {
        alert.show(title: "Headline 6",
                   info: "Apparently we had reached a great height in the atmosphere, for the...",
                   titleOk: "OK",
                   titleCancel: "Cancel",
                   completionOk: nil,
                   completionCancel: nil)
    }

    func didTapDialogWithoutTitle() {
        alert.show(info: "Apparently we had reached a great height in the atmosphere, for the...",
                   titleOk: "OK",
                   titleCancel: "Cancel",
                   completionOk: nil,
                   completionCancel: nil)
    }

    func didTapDialogWithoutDesc() {
        alert.show(title: "Headline 6",
                   titleOk: "OK",
                   titleCancel: "Cancel",
                   completionOk: nil,
                   completionCancel: nil)
    }
}
