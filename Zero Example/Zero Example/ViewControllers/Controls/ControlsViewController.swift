//
//  ControlsViewController.swift
//  designsystem
//
//  Created by Rubén Alonso on 12/05/2020.
//  Copyright © 2020 Batura. All rights reserved.
//

import UIKit
import Zero

class ControlsViewController: ZeroViewController {

    @IBOutlet weak var tableView: UITableView!

    var data = [
        "Switch",
        "SimpleSelection",
        "MultipleSelection"
    ]

    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
    }

    func setupTableView() {
        tableView.register(SwitchCell.nib(), forCellReuseIdentifier: SwitchCell.reuseID())
        tableView.register(SimpleTableViewCell.nib(), forCellReuseIdentifier: SimpleTableViewCell.reuseID())
        tableView.dataSource = self
        tableView.delegate = self
        tableView.rowHeight = UITableView.automaticDimension
        tableView.tableFooterView = UIView()
    }
}

extension ControlsViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: SwitchCell.reuseID(),
                                                     for: indexPath) as? SwitchCell
            cell?.configureWith(item: data[indexPath.row])
            return cell!
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: SimpleTableViewCell.reuseID(),
                                                     for: indexPath) as? SimpleTableViewCell
            cell?.configureWith(item: data[indexPath.row])
            return cell!
        }
    }
}

extension ControlsViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row > 0 {
            let vc = SelectionViewController()
            vc.multipleSelection = (indexPath.row == 2)
            navigationController?.pushViewController(vc, animated: true)
        } else {
            let cell = tableView.cellForRow(at: indexPath) as? SwitchCell
            let change = !(cell?.switchZero.isOn ?? true)
            cell?.switchZero.setOn(change, animated: true)
        }
    }
}
