//
//  SimpleTableViewCell.swift
//  designsystem
//
//  Created by Rubén Alonso on 13/05/2020.
//  Copyright © 2020 Batura. All rights reserved.
//

import UIKit
import Zero

class SimpleTableViewCell: UITableViewCell, TableViewCell {
    typealias Object = String

    var item: String?
    weak var delegate: TableViewCellDelegate?

    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelDesc: UILabel!
    @IBOutlet weak var imageViewLeft: UIImageView!
    @IBOutlet weak var viewImageLeft: UIView!
    @IBOutlet weak var buttonRight: ZeroTextButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        setupStyle()
        accessoryType = .disclosureIndicator
    }

    override func prepareForReuse() {
        super.prepareForReuse()
    }

    func setupStyle() {
        selectionStyle = .none
        tintColor = ZeroColor.primary
        labelDesc.apply(ZeroTheme.LabelFonts.caption1Regular)
        labelTitle.apply(ZeroTheme.LabelFonts.body1Regular)
    }

    func configureWith(item: String?) {
        labelTitle.text = item
        labelDesc.text = item
    }

    @IBAction func didTapActionButton(_ sender: String) {
        delegate?.didTapActionButton(item: item)
    }
}
