//
//  SwitchCell.swift
//  designsystem
//
//  Created by Rubén Alonso on 12/05/2020.
//  Copyright © 2020 Batura. All rights reserved.
//

import UIKit
import Zero

class SwitchCell: UITableViewCell, TableViewCell {
    typealias Object = String
    var item: String?
    weak var delegate: TableViewCellDelegate?

    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelDesc: UILabel!
    @IBOutlet weak var imageViewLeft: UIImageView!
    @IBOutlet weak var viewImageLeft: UIView!
    @IBOutlet weak var buttonRight: ZeroTextButton!

    var switchZero: ZeroSwitch!

    override func awakeFromNib() {
        super.awakeFromNib()
        setupStyle()
        switchZero = ZeroSwitch()
        accessoryView = switchZero
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        switchZero = ZeroSwitch()
    }

    func setupStyle() {
        selectionStyle = .none
        labelDesc.apply(ZeroTheme.LabelFonts.caption1Regular)
        labelTitle.apply(ZeroTheme.LabelFonts.body1Regular)
    }

    func configureWith(item: String?) {
        labelTitle.text = item
        labelDesc.text = item
    }

    @IBAction func didTapActionButton(_ sender: String) {
        delegate?.didTapActionButton(item: item)
    }
}
