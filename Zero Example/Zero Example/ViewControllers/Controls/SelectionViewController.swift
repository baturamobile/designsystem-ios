//
//  SelectionViewController.swift
//  designsystem
//
//  Created by Rubén Alonso on 13/05/2020.
//  Copyright © 2020 Batura. All rights reserved.
//

import UIKit
import Zero

class SelectionViewController: ZeroViewController {

    @IBOutlet weak var tableView: UITableView!
    var multipleSelection = false

    let data = [
        "Option 1",
        "Option 2",
        "Option 3"
    ]
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
    }

    func setupTableView() {
        tableView.allowsMultipleSelection = multipleSelection
        tableView.register(SelectionCell.nib(), forCellReuseIdentifier: SelectionCell.reuseID())
        tableView.dataSource = self
        tableView.rowHeight = UITableView.automaticDimension
        tableView.tableFooterView = UIView()
    }
}

extension SelectionViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: SelectionCell.reuseID(),
                                                 for: indexPath) as? SelectionCell
        cell?.configureWith(item: data[indexPath.row])
        return cell!
    }
}
