//
//  SearchBarViewController.swift
//  designsystem
//
//  Created by Rubén Alonso on 17/04/2019.
//  Copyright © 2019 Batura. All rights reserved.
//

import UIKit
import Zero

class SearchBarViewController: ZeroViewController {

    @IBOutlet weak var tableView: UITableView!

    var searchController: UISearchController?

    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Search"
        searchController = UISearchController(searchResultsController: nil)
        if #available(iOS 11.0, *) {
            navigationItem.searchController = searchController
        } else {
            tableView.tableHeaderView = searchController?.searchBar
        }
        definesPresentationContext = true
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        searchController?.searchBar.apply(ZeroTheme.SearchBarStyle.searchBar)
    }
}
