//
//  BottomBarViewController.swift
//  designsystem
//
//  Created by Rubén Alonso on 27/03/2019.
//  Copyright © 2019 Batura. All rights reserved.
//

import UIKit
import Zero

class BottomBarViewController: TabBarViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        tabBar.isTranslucent = false

        let toolbarVC = ToolbarViewController()
        toolbarVC.tabBarItem = UITabBarItem(title: "Label", image: #imageLiteral(resourceName: "circle.pdf"), selectedImage: #imageLiteral(resourceName: "circle.pdf"))
        let toolbarVC1 = ToolbarViewController()
        toolbarVC1.tabBarItem = UITabBarItem(title: "Label", image: #imageLiteral(resourceName: "circle.pdf"), selectedImage: #imageLiteral(resourceName: "circle.pdf"))
        let toolbarVC2 = ToolbarViewController()
        toolbarVC2.tabBarItem = UITabBarItem(title: "Label", image: #imageLiteral(resourceName: "circle.pdf"), selectedImage: #imageLiteral(resourceName: "circle.pdf"))
        let toolbarVC3 = ToolbarViewController()
        toolbarVC3.tabBarItem = UITabBarItem(title: "Label", image: #imageLiteral(resourceName: "circle.pdf"), selectedImage: #imageLiteral(resourceName: "circle.pdf"))
        setViewControllers([toolbarVC, toolbarVC1, toolbarVC2, toolbarVC3], animated: true)

        badge(value: "1", tabIndex: 0)
        badge(value: "Test", tabIndex: 1)
        badge(value: " ", tabIndex: 2)

        let item = UIBarButtonItem(title: "Alternative",
                                   style: .plain,
                                   target: self,
                                   action: #selector(changeTheme))
        navigationItem.rightBarButtonItems = [item]
    }

    @objc func changeTheme() {
        theme = theme == .primary ? .default : .primary
    }
}
