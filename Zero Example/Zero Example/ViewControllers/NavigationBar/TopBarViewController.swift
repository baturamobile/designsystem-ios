//
//  TopBarViewController.swift
//  designsystem
//
//  Created by Rubén Alonso on 27/03/2019.
//  Copyright © 2019 Batura. All rights reserved.
//

import UIKit
import Zero

class TopBarViewController: ZeroViewController {

    @IBOutlet weak var tableView: UITableView!

    let items : [(text: String, vc: UIViewController)] = [("Title Center", TopCenterViewController()),
                                                          ("Alternative - Primary", TopLeftViewController()),
                                                          ("Large Title", LargeTitleViewController())]

    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
    }

    func setupTableView() {
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "CellID")
        tableView.dataSource = self
        tableView.delegate = self
        tableView.tableFooterView = UIView()
    }
}

// MARK: - UITableViewDataSource
extension TopBarViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellID",
                                                 for: indexPath)
        cell.textLabel?.text = items[indexPath.row].text
        cell.textLabel?.font = .regular(size: .body1)
        return cell
    }
}

extension TopBarViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let item = items[indexPath.row]
        navigationController?.pushViewController(item.vc, animated: true)
    }
}
