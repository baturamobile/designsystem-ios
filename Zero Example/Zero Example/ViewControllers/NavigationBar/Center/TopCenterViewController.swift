//
//  TopCenterViewController.swift
//  designsystem
//
//  Created by Rubén Alonso on 27/03/2019.
//  Copyright © 2019 Batura. All rights reserved.
//

import UIKit
import Zero

class TopCenterViewController: ZeroViewController {

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        title = "Center"
        preferLargeTitles = false
        let item = UIBarButtonItem(image: #imageLiteral(resourceName: "circle"), style: .plain, target: nil, action: nil)
        let item2 = UIBarButtonItem(image: #imageLiteral(resourceName: "circle"), style: .plain, target: nil, action: nil)
        navigationItem.rightBarButtonItems = [item, item2]
    }
}
