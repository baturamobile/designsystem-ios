//
//  LargeTitleViewController.swift
//  designsystem
//
//  Created by Rubén Alonso on 27/03/2019.
//  Copyright © 2019 Batura. All rights reserved.
//

import UIKit
import Zero

class LargeTitleViewController: ZeroViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Large title"

        let item = UIBarButtonItem(image: #imageLiteral(resourceName: "circle"), style: .plain, target: nil, action: nil)
        let item2 = UIBarButtonItem(title: "Label", style: .plain, target: nil, action: nil)
        item2.tintColor = ZeroColor.onSurface()
        item2.font = ZeroFont.bold(size: .body2)
        navigationItem.rightBarButtonItems = [item, item2]
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        preferLargeTitles = true
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        preferLargeTitles = false
    }
}
