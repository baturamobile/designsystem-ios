//
//  TopLeftViewController.swift
//  designsystem
//
//  Created by Rubén Alonso on 27/03/2019.
//  Copyright © 2019 Batura. All rights reserved.
//

import UIKit
import Zero

class TopLeftViewController: ZeroViewController {

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        title = "Alternative - Primary"
        theme = .primary
        let item = UIBarButtonItem(title: "Label", style: .plain, target: nil, action: nil)
        navigationItem.rightBarButtonItems = [item]
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillAppear(animated)
        theme = .default
    }
}
