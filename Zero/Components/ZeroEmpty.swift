//
//  ZeroEmpty.swift
//  designsystem
//
//  Created by Rubén Alonso on 11/06/2020.
//  Copyright © 2020 Batura. All rights reserved.
//

import UIKit

public class ZeroEmpty: UIView {

    public var title: String? = nil {
        didSet { update() }
    }
    public var desc: String? = nil {
        didSet { update() }
    }
    public var buttonText: String? = nil {
        didSet { update() }
    }
    public var image: UIImage? = nil {
        didSet { update() }
    }
    public var buttonAction: (() -> Void)? = nil {
        didSet { update() }
    }

    private let imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.heightAnchor.constraint(lessThanOrEqualToConstant: 200).isActive = true
        return imageView
    }()

    private let titleLabel: UILabel = {
        let label = UILabel()
        label.apply([ZeroTheme.LabelFonts.body2])
        label.textAlignment = .center
        label.numberOfLines = 0
        return label
    }()

    private let descLabel: UILabel = {
        let label = UILabel()
        label.apply([ZeroTheme.LabelFonts.body2Light])
        label.textAlignment = .center
        label.numberOfLines = 0
        return label
    }()

    private let button: ZeroTextButton = {
        let button = ZeroTextButton()
        button.style = .normal
        return button
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }

    private func commonInit() {
        backgroundColor = .background

        update()

        let stackview = UIStackView()
        stackview.axis = .vertical
        stackview.distribution = .fill
        stackview.alignment = .fill
        stackview.spacing = 36

        let stackviewLabels = UIStackView()
        stackviewLabels.axis = .vertical
        stackviewLabels.distribution = .fill
        stackviewLabels.alignment = .fill
        stackviewLabels.spacing = 8

        stackviewLabels.addArrangedSubview(titleLabel)
        stackviewLabels.addArrangedSubview(descLabel)

        stackview.addArrangedSubview(imageView)
        stackview.addArrangedSubview(stackviewLabels)
        stackview.addArrangedSubview(button)

        addSubview(stackview)
        stackview.translatesAutoresizingMaskIntoConstraints = false

        var safeAreaBottom = bottomAnchor
        if #available(iOS 11.0, *) {
            safeAreaBottom = safeAreaLayoutGuide.bottomAnchor
        }

        NSLayoutConstraint.activate([
            stackview.topAnchor.constraint(equalTo: topAnchor, constant: 48),
            stackview.bottomAnchor.constraint(lessThanOrEqualTo: safeAreaBottom, constant: -16),
            stackview.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 32),
            stackview.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -32)
        ])
    }

    private func update() {
        imageView.isHidden = image == nil
        titleLabel.isHidden = title == nil
        descLabel.isHidden = desc == nil
        button.isHidden = buttonText == nil

        imageView.image = image
        titleLabel.text = title
        descLabel.text = desc
        button.setTitle(buttonText, for: .normal)

        button.completion(buttonAction)
    }

    public func show(inView: UIView) {
        frame = inView.bounds
        autoresizingMask = [.flexibleWidth, .flexibleHeight]
        inView.addSubview(self)
    }

    public func hide() {
        removeFromSuperview()
    }
}
