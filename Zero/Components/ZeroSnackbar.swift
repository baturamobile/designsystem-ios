//
//  Snackbar.swift
//  designsystem
//
//  Created by Rubén Alonso on 25/04/2019.
//  Copyright © 2019 Batura. All rights reserved.
//

import UIKit

public struct ZeroSnackbarAction {
    var title: String
    var action: (() -> Void)?

    public init(title: String, action: (() -> Void)? = nil) {
        self.title = title
        self.action = action
    }
}

public class ZeroSnackbar: UIView {
    private var button: UIButton = UIButton()
    private var label: UILabel = UILabel()

    private var bottomConst: NSLayoutConstraint?
    private var labelRightConst: NSLayoutConstraint?

    public var textColor: UIColor = ZeroColor.background {
        didSet { updateStyle() }
    }

    public var textFont: UIFont = ZeroFont.regular(size: .body2) {
        didSet { updateStyle() }
    }

    public var buttonColor: UIColor = ZeroColor.primary {
        didSet { updateStyle() }
    }

    public var backColor: UIColor? = ZeroColor.onBackground() {
        didSet { updateStyle() }
    }

    public var buttonFont: UIFont = ZeroFont.bold(size: .button2) {
        didSet { updateStyle() }
    }

    public var text: String = "" {
        didSet { configure() }
    }
    public var action: ZeroSnackbarAction? {
        didSet { configure() }
    }

    public var viewBottom: UIView?

    public override init(frame: CGRect) {
        super.init(frame: frame)
        updateStyle()
        setupView()
    }

    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        updateStyle()
        setupView()
    }

    public init(text: String? = nil, action: ZeroSnackbarAction? = nil) {
        super.init(frame: CGRect.zero)
        self.text = text ?? ""
        self.action = action
        updateStyle()
        setupView()
    }

    private func updateStyle() {
        button.setTitleColor(buttonColor, for: .normal)
        button.titleLabel?.font = buttonFont
        label.textColor = textColor
        label.font = textFont
        backgroundColor = backColor
    }

    private func setupAccesibility() {
        accessibilityElements = [label, button]
    }

    private func setupLabel() {
        label.numberOfLines = 0
        label.setContentCompressionResistancePriority(.required,
                                                      for: .vertical)
        label.setContentHuggingPriority(UILayoutPriority(249),
                                        for: .horizontal)
        label.translatesAutoresizingMaskIntoConstraints = false
        addSubview(label)

        NSLayoutConstraint.activate([
            label.leftAnchor.constraint(equalTo: leftAnchor, constant: 24),
            label.topAnchor.constraint(equalTo: topAnchor, constant: 16),
            label.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -16)
        ])
        labelRightConst = label.rightAnchor.constraint(equalTo: rightAnchor, constant: -24)
    }

    private func setupButton() {
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setContentCompressionResistancePriority(.required,
                                                       for: .horizontal)
    }

    private func setupView() {
        translatesAutoresizingMaskIntoConstraints = false
        layer.cornerRadius = 5
        setupAccesibility()
        setupLabel()
        setupButton()
        configure()
    }

    private func configure() {
        label.text = text

        button.removeFromSuperview()
        button.setTitle(action?.title, for: .normal)
        button.addTarget(self, action: #selector(handleAction), for: .touchUpInside)
        if action != nil {
            addSubview(button)
            labelRightConst?.isActive = false
            NSLayoutConstraint.activate([
                button.leftAnchor.constraint(equalTo: label.rightAnchor, constant: 16),
                button.centerYAnchor.constraint(equalTo: centerYAnchor),
                button.rightAnchor.constraint(equalTo: rightAnchor, constant: -24)
            ])
        } else {
            labelRightConst?.isActive = true
        }
        sizeToFit()
    }

    @objc func handleAction() {
        action?.action?()
        hide()
    }

    public override func didMoveToSuperview() {
        super.didMoveToSuperview()
        guard let superview = superview else {
            return
        }
        var safeAreaBottom = superview.bottomAnchor
        var safeAreaLeading = superview.leadingAnchor
        var safeAreaTrailing = superview.trailingAnchor
        var safeAreaTop = superview.topAnchor
        if #available(iOS 11.0, *) {
            safeAreaBottom = superview.safeAreaLayoutGuide.bottomAnchor
            safeAreaLeading = superview.safeAreaLayoutGuide.leadingAnchor
            safeAreaTrailing = superview.safeAreaLayoutGuide.trailingAnchor
            safeAreaTop = superview.safeAreaLayoutGuide.topAnchor
        }
        let bottomAnc = viewBottom != nil ? viewBottom!.topAnchor : safeAreaBottom

        NSLayoutConstraint.activate([
            leadingAnchor.constraint(equalTo: safeAreaLeading, constant: 8),
            trailingAnchor.constraint(equalTo: safeAreaTrailing, constant: -8),
            bottomAnchor.constraint(equalTo: bottomAnc, constant: -8),
            topAnchor.constraint(greaterThanOrEqualTo: safeAreaTop, constant: 8)
        ])

        isHidden = true
        layer.opacity = 0
        self.transform = transform.scaledBy(x: 0.8, y: 0.8)
    }

    public func show(hideAfter: TimeInterval? = nil) {
        isHidden = false
        UIView.animate(withDuration: ZeroConstants.Animation.timing) {
            self.layer.opacity = 1
            self.transform = .identity
            self.superview!.layoutIfNeeded()
            UIAccessibility.post(notification: .layoutChanged, argument: self)
        }

        if let hideAfter = hideAfter {
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + hideAfter) {
                self.hide()
            }
        }
    }

    public func hide() {
        UIView.animate(withDuration: ZeroConstants.Animation.timing, animations: {
            self.layer.opacity = 0
            self.transform = self.transform.scaledBy(x: 0.8, y: 0.8)
        }, completion: { (_) in
            self.isHidden = true
        })
    }
}
