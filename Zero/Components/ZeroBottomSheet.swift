//
//  BottonSheetController.swift
//  designsystem
//
//  Created by Rubén Alonso on 04/03/2020.
//  Copyright © 2020 Batura. All rights reserved.
//

import UIKit

public struct ZeroBottomSheetAction {
    var title: String
    var image: UIImage
    var style: UIAlertAction.Style = .default
    var handler: (() -> Void)?

    public init(title: String,
                image: UIImage,
                style: UIAlertAction.Style = .default,
                handler: (() -> Void)? = nil) {

        self.title = title
        self.image = image
        self.style = style
        self.handler = handler
    }
}

public class ZeroBottomSheet: UIViewController {

    public var spacing: CGFloat = 0.0
    public var topTitle: String?
    public var itemHeight: CGFloat = 48.0
    public var titleHeight: CGFloat = 36.0

    private var actions = [ZeroBottomSheetAction]()

    private var stackView = UIStackView()
    private var optionsView = UIView()

    private var topConstraint: NSLayoutConstraint!
    private var bottomConstraint: NSLayoutConstraint!

    public init(title: String? = nil) {
        super.init(nibName: nil, bundle: nil)
        topTitle = title
        modalPresentationStyle = .overCurrentContext
    }

    public required init?(coder: NSCoder) {
        super.init(coder: coder)
    }

    public override func viewDidLoad() {
        super.viewDidLoad()
        initView()
        insertActions()
    }

    public override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        show()
    }

    public func add(action: ZeroBottomSheetAction) {
        if action.style != .destructive {
            actions.insert(action, at: actions.count)
        } else {
            actions.append(action)
        }
    }

    private func insertActions() {
        for (index, action) in actions.enumerated() {
            let button = ZeroTextButton()
            button.apply(ZeroTheme.Button.bottomSheet)
            button.completion {
                self.hide(callback: action.handler)
            }

            button.setTitle(action.title, for: .normal)
            button.setImage(action.image.withRenderingMode(.alwaysTemplate), for: .normal)
            button.heightAnchor.constraint(equalToConstant: itemHeight).isActive = true

            if action.style == .destructive {
                button.style = .error
                button.tintColor = ZeroColor.error
            }

            if index < actions.count - 1 {
                button.addBorder(borders: [.bottom], color: .onSurface(.custom(value: 0.12)))
            }

            stackView.addArrangedSubview(button)
        }
    }

    func initView() {
        setupOptionsView()
        setupStackView()
        setupTitle()
        setupStyle()
    }

    func setupStyle() {
        view.frame = UIScreen.main.bounds
        view.backgroundColor = ZeroColor.black.withAlphaComponent(ZeroConstants.Theme.backdropAlpha)
        let tap = UITapGestureRecognizer(target: self, action: #selector(dismissController))
        view.addGestureRecognizer(tap)

        optionsView.backgroundColor = .surface
        optionsView.cornerRadius = 5
        optionsView.shadow(offset: CGSize(width: 0, height: -2))
    }

    private func show() {
        bottomConstraint.priority = UILayoutPriority(751)
        UIView.animate(withDuration: ZeroConstants.Animation.timing, animations: {
            self.view.layoutIfNeeded()
        }, completion: { _ in
            UIAccessibility.post(notification: .screenChanged, argument: self.optionsView)
        })
    }

    private func hide(callback: (() -> Void)? = nil) {
        bottomConstraint.priority = .defaultLow
        UIView.animate(withDuration: ZeroConstants.Animation.timing, animations: {
            self.view.layoutIfNeeded()
        }, completion: { (_) in
            self.dismiss(animated: false, completion: nil)
            callback?()
        })
    }

    @objc private func dismissController() {
        hide {
            self.dismiss(animated: false, completion: nil)
        }
    }

    private func setupTitle() {
        guard let title = topTitle else {
            return
        }
        let label = UILabel()
        label.textAlignment = .center
        label.apply(ZeroTheme.LabelFonts.body1)
        label.text = title
        label.heightAnchor.constraint(equalToConstant: titleHeight).isActive = true
        stackView.addArrangedSubview(label)
    }

    private func setupOptionsView() {
        view.addSubview(optionsView)
        optionsView.translatesAutoresizingMaskIntoConstraints = false

        var safeAreaLeading = view.leadingAnchor
        var safeAreaTrailing = view.trailingAnchor
        if #available(iOS 11.0, *) {
            safeAreaLeading = view.safeAreaLayoutGuide.leadingAnchor
            safeAreaTrailing = view.safeAreaLayoutGuide.trailingAnchor
        }

        NSLayoutConstraint.activate([
            optionsView.trailingAnchor.constraint(equalTo: safeAreaTrailing),
            optionsView.leadingAnchor.constraint(equalTo: safeAreaLeading)
        ])

        topConstraint = optionsView.topAnchor.constraint(equalTo: view.bottomAnchor, constant: 0)
        topConstraint.priority = .defaultHigh
        topConstraint.isActive = true

        bottomConstraint = optionsView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0)
        bottomConstraint.priority = .defaultLow
        bottomConstraint.isActive = true
    }

    private func setupStackView() {
        optionsView.addSubview(stackView)
        stackView.axis = .vertical
        stackView.distribution = .fill
        stackView.alignment = .fill
        stackView.spacing = spacing
        stackView.translatesAutoresizingMaskIntoConstraints = false

        var safeAreaBottom = view.bottomAnchor
        if #available(iOS 11.0, *) {
            safeAreaBottom = view.safeAreaLayoutGuide.bottomAnchor
        }

        NSLayoutConstraint.activate([
            stackView.topAnchor.constraint(equalTo: optionsView.topAnchor, constant: 16),
            stackView.leadingAnchor.constraint(equalTo: optionsView.leadingAnchor, constant: 24),
            stackView.trailingAnchor.constraint(equalTo: optionsView.trailingAnchor, constant: -24),
            stackView.bottomAnchor.constraint(equalTo: safeAreaBottom, constant: 0)
        ])
    }
}
