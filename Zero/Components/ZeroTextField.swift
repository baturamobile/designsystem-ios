//
//  TextInput.swift
//  designsystem
//
//  Created by Rubén Alonso on 30/04/2019.
//  Copyright © 2019 Batura. All rights reserved.
//

import MaterialComponents

public class ZeroTextField: MDCTextField {
    public override var isSecureTextEntry: Bool {
        didSet {
            if isSecureTextEntry {
                configureSecureText()
            }
        }
    }

    func configureSecureText() {
        let button = UIButton(frame: CGRect(origin: CGPoint.zero,
                                            size: CGSize(width: 44, height: 44)))
        let image = UIImage(named: "eyeOpen", in: Bundle(for: ZeroTextField.self), compatibleWith: nil)
        button.setImage(image, for: .normal)
        button.addTarget(self, action: #selector(changeSecureText), for: .touchUpInside)
        trailingView = button
        trailingViewMode = .whileEditing
    }

    @objc func changeSecureText() {
        let button = (trailingView as? UIButton)
        if isSecureTextEntry {
            let image = UIImage(named: "eyeClose", in: Bundle(for: ZeroTextField.self), compatibleWith: nil)
            button?.setImage(image, for: .normal)
        }
        isSecureTextEntry.toggle()
    }
}

extension MDCTextInputControllerBase {
    private struct Validation {
        static var validate: (() -> Bool)?
    }

    public var validationCompletion: (() -> Bool)? {
        get {
            return Validation.validate
        }
        set {
            Validation.validate = newValue
        }
    }

    public var isValid: Bool {
        return errorText == nil
    }

    @discardableResult
    public func validate() -> Bool {
        if let validation = validationCompletion {
            return validation()
        }
        return true
    }
}

public class ZeroTextFieldControllerFilled: MDCTextInputControllerFilled {

    public var colorError: UIColor? = ZeroColor.error {
        didSet { update() }
    }
    public var colorActive: UIColor? = ZeroColor.primary {
        didSet { update() }
    }
    public var colorPlaceholder: UIColor? = ZeroColor.onBackground(.low) {
        didSet { update() }
    }
    public var colorNormal: UIColor? = ZeroColor.onBackground() {
        didSet { update() }
    }
    public var font: UIFont? = ZeroFont.light(size: .body2) {
        didSet { update() }
    }
    public var fontPlaceholder: UIFont? = ZeroFont.light(size: .body2) {
        didSet { update() }
    }
    public var fontError: UIFont? = ZeroFont.regular(size: .caption2) {
        didSet { update() }
    }

    public override init() {
        super.init()
    }

    public required init(textInput input: (UIView & MDCTextInput)?) {
        super.init(textInput: input)
        update()
    }

    func update() {
        errorColor = colorError
        normalColor = colorNormal
        activeColor = colorActive
        disabledColor = colorPlaceholder

        inlinePlaceholderColor = colorPlaceholder
        inlinePlaceholderFont = font

        floatingPlaceholderErrorActiveColor = errorColor
        floatingPlaceholderNormalColor = colorNormal
        floatingPlaceholderActiveColor = colorActive

        textInputFont = font
        leadingUnderlineLabelFont = fontError
        trailingUnderlineLabelFont = fontError
    }
}

public class ZeroTextFieldControllerOutlined: MDCTextInputControllerOutlined {

    public var colorError: UIColor? = ZeroColor.error {
        didSet { update() }
    }
    public var colorActive: UIColor? = ZeroColor.primary {
        didSet { update() }
    }
    public var colorPlaceholder: UIColor? = ZeroColor.onBackground(.low) {
        didSet { update() }
    }
    public var colorNormal: UIColor? = ZeroColor.onBackground() {
        didSet { update() }
    }
    public var font: UIFont? = ZeroFont.light(size: .body2) {
        didSet { update() }
    }
    public var fontPlaceholder: UIFont? = ZeroFont.light(size: .head2) {
        didSet { update() }
    }
    public var fontError: UIFont? = ZeroFont.regular(size: .caption2) {
        didSet { update() }
    }

    public override init() {
        super.init()
    }

    public required init(textInput input: (UIView & MDCTextInput)?) {
        super.init(textInput: input)
        update()
    }

    func update() {
        errorColor = colorError
        normalColor = colorNormal
        activeColor = colorActive
        disabledColor = colorPlaceholder

        inlinePlaceholderColor = colorPlaceholder
        inlinePlaceholderFont = font

        floatingPlaceholderErrorActiveColor = errorColor
        floatingPlaceholderNormalColor = colorNormal
        floatingPlaceholderActiveColor = colorActive

        textInputFont = font
        leadingUnderlineLabelFont = fontError
        trailingUnderlineLabelFont = fontError
    }
}

public class PaddingTextField: UITextField {

    @IBInspectable
    var padding: CGFloat = 16

    private var insets: UIEdgeInsets

    override init(frame: CGRect) {
        insets = UIEdgeInsets(top: 0, left: padding, bottom: 0, right: padding)
        super.init(frame: frame)
    }

    required init?(coder aDecoder: NSCoder) {
        insets = UIEdgeInsets(top: 0, left: padding, bottom: 0, right: padding)
        super.init(coder: aDecoder)
    }

    public override func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: insets)
    }
    public override func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: insets)
    }
    public override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: insets)
    }
}
