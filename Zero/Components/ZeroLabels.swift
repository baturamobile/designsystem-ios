//
//  Labels.swift
//  designsystem
//
//  Created by Rubén Alonso on 07/05/2019.
//  Copyright © 2019 Batura. All rights reserved.
//

import Foundation
import UIKit

public class ZeroPaddingLabel: UILabel {

    @IBInspectable
    var padding: CGFloat = 10

    public override var intrinsicContentSize: CGSize {
        var size = super.intrinsicContentSize
        size.width += padding
        return size
    }
}
