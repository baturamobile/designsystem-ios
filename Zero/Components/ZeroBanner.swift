//
//  Banner.swift
//  designsystem
//
//  Created by Rubén Alonso on 14/02/2020.
//  Copyright © 2020 Batura. All rights reserved.
//

import UIKit

@objc public protocol ZeroBannerDelegate {
    @objc optional func bannerWillAppear(banner: ZeroBanner)
    @objc optional func bannerDidAppear(banner: ZeroBanner)
    @objc optional func bannerWillDismiss(banner: ZeroBanner)
    @objc optional func bannerDidDismiss(banner: ZeroBanner)
}

public class ZeroBanner: UIView {

    private var imageView: UIImageView!
    private var labelText: UILabel!
    private var buttonOk: ZeroTextButton!
    private var buttonCancel: ZeroTextButton!
    private var viewButtons: UIView!
    private var viewTop: UIStackView!

    public weak var delegate: ZeroBannerDelegate?

    private var constraintHeight: NSLayoutConstraint?

    public var image: UIImage? {
        didSet { update() }
    }
    public var text: String? {
        didSet { update() }
    }
    public var buttonOkText: String? {
        didSet { update() }
    }
    public var buttonCancelText: String? {
        didSet { update() }
    }
    public var bgColor: UIColor = ZeroColor.surface {
        didSet { setupStyle() }
    }

    public var completionOk: (() -> Void)?
    public var completionCancel: (() -> Void)?

    public override init(frame: CGRect) {
        super.init(frame: frame)
        customInitialize()
        setupStyle()
    }

    public required init?(coder: NSCoder) {
        super.init(coder: coder)
        customInitialize()
        setupStyle()
    }

    func customInitialize() {

        viewTop = UIStackView()
        viewTop.translatesAutoresizingMaskIntoConstraints = false
        viewTop.axis = .horizontal
        viewTop.alignment = .top
        viewTop.spacing = 16
        addSubview(viewTop)

        imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        viewTop.addArrangedSubview(imageView)

        labelText = UILabel()
        labelText.translatesAutoresizingMaskIntoConstraints = false
        labelText.numberOfLines = 0
        labelText.setContentCompressionResistancePriority(.required, for: .vertical)
        viewTop.addArrangedSubview(labelText)

        viewButtons = UIView()
        viewButtons.translatesAutoresizingMaskIntoConstraints = false
        addSubview(viewButtons)

        buttonOk = ZeroTextButton()
        buttonOk.translatesAutoresizingMaskIntoConstraints = false
        buttonCancel = ZeroTextButton()
        buttonCancel.translatesAutoresizingMaskIntoConstraints = false

        viewButtons.addSubview(buttonOk)
        viewButtons.addSubview(buttonCancel)

        buttonOk.completion {
            self.hide()
            self.completionOk?()
        }
        buttonCancel.completion {
            self.hide()
            self.completionCancel?()
        }

        isHidden = true
        setupConstraints()
    }

    private func setupStyle() {
        backgroundColor = bgColor
        buttonOk.style = .normal
        buttonCancel.style = .normal
        labelText.apply(ZeroTheme.LabelFonts.body2Regular)
        labelText.textColor = ZeroColor.onSurface()

        shadow()
    }

    private func setupConstraints() {
        imageView.widthAnchor.constraint(equalToConstant: 44).isActive = true

        NSLayoutConstraint.activate([
            viewTop.topAnchor.constraint(equalTo: topAnchor, constant: 16),
            viewTop.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 16),
            viewTop.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -16)
        ])

        NSLayoutConstraint.activate([
            viewButtons.topAnchor.constraint(equalTo: viewTop.bottomAnchor, constant: 8),
            viewButtons.leadingAnchor.constraint(equalTo: labelText.leadingAnchor, constant: 0),
            viewButtons.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -16),
            viewButtons.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -8)
        ])

        NSLayoutConstraint.activate([
            buttonOk.topAnchor.constraint(equalTo: viewButtons.topAnchor, constant: 8),
            buttonOk.trailingAnchor.constraint(equalTo: viewButtons.trailingAnchor, constant: 0),
            buttonOk.bottomAnchor.constraint(equalTo: viewButtons.bottomAnchor, constant: 0)
        ])

        NSLayoutConstraint.activate([
            buttonCancel.trailingAnchor.constraint(equalTo: buttonOk.leadingAnchor, constant: 16),
            buttonCancel.centerYAnchor.constraint(equalTo: buttonOk.centerYAnchor, constant: 0)
        ])
    }

    private func update() {
        imageView.isHidden = (image == nil)
        imageView.image = image
        labelText.text = text

        buttonCancel.isHidden = (buttonCancelText == nil)
        buttonOk.setTitle(buttonOkText, for: .normal)
        buttonCancel.setTitle(buttonCancelText, for: .normal)

        layoutIfNeeded()
    }

    public override func didMoveToSuperview() {
        super.didMoveToSuperview()
        guard let superview = superview else {
            return
        }

        var safeAreaLeading = superview.leadingAnchor
        var safeAreaTrailing = superview.trailingAnchor
        var safeAreaTop = superview.topAnchor
        if #available(iOS 11.0, *) {
            safeAreaLeading = superview.safeAreaLayoutGuide.leadingAnchor
            safeAreaTrailing = superview.safeAreaLayoutGuide.trailingAnchor
            safeAreaTop = superview.safeAreaLayoutGuide.topAnchor
        }

        translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            topAnchor.constraint(equalTo: safeAreaTop, constant: 0),
            leadingAnchor.constraint(equalTo: safeAreaLeading, constant: 0),
            trailingAnchor.constraint(equalTo: safeAreaTrailing, constant: 0)
        ])

        superview.bringSubviewToFront(self)
    }

    public func show() {
        delegate?.bannerWillAppear?(banner: self)
        isHidden = false
        delegate?.bannerDidAppear?(banner: self)
        UIAccessibility.post(notification: .layoutChanged, argument: self)
    }

    public func hide() {
        delegate?.bannerWillDismiss?(banner: self)
        isHidden = true
        delegate?.bannerDidDismiss?(banner: self)
    }
}
