//
//  AlertViewController.swift
//  designsystem
//
//  Created by Rubén Alonso on 11/06/2019.
//  Copyright © 2019 Batura. All rights reserved.
//

import UIKit

public protocol ZeroDialogTableDelegate: class {
    func didTapItem(item: (UIImage, String))
}

public class ZeroDialog: UIViewController {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelInfo: UILabel!
    @IBOutlet weak var btnOk: ZeroTextButton!
    @IBOutlet weak var btnCancel: ZeroTextButton!

    public weak var delegate: ZeroDialogTableDelegate?

    private let cellId = "AlertViewControllerCellId"
    private var items = [(image: UIImage, text:String)]()

    public init() {
        super.init(nibName: "ZeroDialog", bundle: Bundle(for: ZeroDialog.self))
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }

    public override func viewDidLoad() {
        super.viewDidLoad()
        setupStyle()
    }

    func setupStyle() {

        ZeroTheme.Button.normal.apply(to: [btnOk,
                                       btnCancel])
        labelTitle.apply([ZeroTheme.LabelFonts.head4])
        labelTitle.textColor = ZeroColor.onSurface()
        labelInfo.apply([ZeroTheme.LabelFonts.body2Regular])
        labelInfo.textColor = ZeroColor.onSurface()
        containerView.shadow()
        containerView.cornerRadius = 5
        containerView.backgroundColor = ZeroColor.surface
    }

    public func show(image: UIImage? = nil,
              title: String = "",
              info: String = "",
              titleOk: String = "",
              titleCancel: String = "",
              completionOk: (() -> Void)? = nil,
              completionCancel: (() -> Void)? = nil) {

        view.backgroundColor = ZeroColor.black.withAlphaComponent(ZeroConstants.Theme.backdropAlpha)

        imageView.image = image
        labelTitle.text = title
        labelInfo.text = info
        btnOk.setTitle(titleOk, for: .normal)
        btnOk.completion {
            self.hide()
            completionOk?()
        }
        btnCancel.setTitle(titleCancel, for: .normal)
        btnCancel.completion {
            self.hide()
            completionCancel?()
        }

        imageView.isHidden = (image == nil)
        labelTitle.isHidden = title.isEmpty
        labelInfo.isHidden = info.isEmpty
        btnOk.isHidden = titleOk.isEmpty
        btnCancel.isHidden = titleCancel.isEmpty

        show()
    }

    private func show(_ inView: UIView? = nil) {
        if let view = inView {
            DispatchQueue.main.async {
                self.view.frame = view.bounds
                view.addSubview(self.view)
                UIAccessibility.post(notification: .screenChanged, argument: self.containerView)
            }
        } else {
            DispatchQueue.main.async {
                self.containerView.center = self.view.center
                let viewController = UIApplication.shared.keyWindow?.rootViewController
                self.modalPresentationStyle = .overFullScreen
                viewController?.present(self, animated: false, completion: nil)
            }
        }
    }

    private func hide() {
        DispatchQueue.main.async {
            let center = CGPoint(x: self.containerView.center.x, y: self.containerView.center.y * 4)
            UIView.animate(withDuration: 0.35, animations: {
                self.containerView.center = center
            }, completion: { (_) in
                self.dismiss(animated: false, completion: nil)
            })
        }
    }
}
