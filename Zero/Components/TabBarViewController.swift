//
//  TabBarViewController.swift
//  designsystem
//
//  Created by Rubén Alonso on 17/04/2019.
//  Copyright © 2019 Batura. All rights reserved.
//

import UIKit

open class TabBarViewController: UITabBarController {

    public var theme: ZeroTheme.NavigationBar = .default {
        didSet { updateStyle() }
    }

    open override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateStyle()
    }

    private func updateStyle() {
        switch theme {
        case .primary:
            tabBar.tintColor = ZeroColor.onPrimary()
            tabBar.barTintColor = ZeroColor.primary
            tabBar.unselectedItemTintColor = .onPrimary(.low)
            tabBar.items?.forEach({ (item) in
                item.badgeColor = .background
                item.setBadgeTextAttributes([.foregroundColor: ZeroColor.primary],
                                            for: .normal)
            })
        default:
            tabBar.tintColor = ZeroColor.primary
            tabBar.barTintColor = ZeroColor.surface
            tabBar.unselectedItemTintColor = .onSurface(.low)
            tabBar.items?.forEach({ (item) in
                item.badgeColor = .primary
                item.setBadgeTextAttributes([.foregroundColor: ZeroColor.onPrimary()],
                                            for: .normal)
            })
        }
    }

    public override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        let color = tabBar.tintColor
        guard let index = tabBar.items?.firstIndex(of: item) else {
            return
        }
        let width = (tabBar.frame.width / CGFloat((tabBar.items?.count)!))
        let xOrigin = (width * CGFloat(index)) + width / 2

        let endFrame = CGRect(x: xOrigin - (width / 2),
                              y: -(width / 4),
                              width: width,
                              height: width)

        let circle = UIView(frame: endFrame)
        circle.layer.cornerRadius = endFrame.height / 2
        circle.backgroundColor = color?.withAlphaComponent(0.2)
        circle.transform = CGAffineTransform(scaleX: 0.3, y: 0.3)

        let view = UIView(frame: tabBar.bounds)
        view.clipsToBounds = true
        tabBar.addSubview(view)
        view.addSubview(circle)

        UIView.animate(withDuration: ZeroConstants.Animation.timing, animations: {
            circle.transform = CGAffineTransform.identity
        }, completion: { _ in
            view.removeFromSuperview()
            circle.removeFromSuperview()
        })
    }

    public func badge(value: String, tabIndex: Int) {
        if let tabBarItem = tabBar.items?[tabIndex] {
            tabBarItem.badgeValue = value
            if #available(iOS 10.0, *) {
                tabBarItem.badgeColor = ZeroColor.error
            }
        }
    }
}
