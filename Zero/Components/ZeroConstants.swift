//
//  Constants.swift
//  designsystem
//
//  Created by Rubén Alonso on 02/04/2020.
//  Copyright © 2020 Batura. All rights reserved.
//

import UIKit

struct ZeroConstants {
    struct Animation {
        static let timing = 0.2
    }
    struct Theme {
        static let backdropAlpha: CGFloat = 0.78
    }
}
