//
//  BaturaTableViewCell.swift
//  designsystem
//
//  Created by Rubén Alonso on 02/07/2019.
//  Copyright © 2019 Batura. All rights reserved.
//

import UIKit

class BaturaTableViewCell: UITableViewCell, TableViewCell {
    typealias Object = Any

    var item: Any?
    weak var delegate: TableViewCellDelegate?

    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelDesc: UILabel!
    @IBOutlet weak var labelInfo: UILabel!
    @IBOutlet weak var imageViewLeft: UIImageView!
    @IBOutlet weak var buttonRight: ZeroTextButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        setupStyle()
    }

    override func prepareForReuse() {
        super.prepareForReuse()
    }

    func setupStyle() {
        labelDesc.apply(ZeroTheme.LabelFonts.body2Light)
        labelInfo.apply(ZeroTheme.LabelFonts.caption2Regular)
        labelTitle.apply(ZeroTheme.LabelFonts.body2)
        labelInfo.textColor = ZeroColor.onBackground(.medium)
        buttonRight.style = .normal
    }

    func configureWith(item: Any?) {

    }

    @IBAction func didTapActionButton(_ sender: Any) {
        delegate?.didTapActionButton(item: item)
    }
}
