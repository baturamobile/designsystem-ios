//
//  TableViewCell.swift
//  designsystem
//
//  Created by Rubén Alonso on 17/06/2019.
//  Copyright © 2019 Batura. All rights reserved.
//

import Foundation
import UIKit

public protocol BasicCell {}

public extension BasicCell {
    static func nib() -> UINib {
        return UINib(nibName: String(describing: Self.self),
                     bundle: nil)
    }
    static func reuseID() -> String {
        return "\(String(describing: Self.self))ID"
    }
}

public protocol TableViewCell: BasicCell {
    associatedtype Object

    var item: Object? {get set}
    var delegate: TableViewCellDelegate? { get set }

    func configureWith(item: Object?)
}

public protocol TableViewCellDelegate: class {
    func didTapActionButton(item: Any?)
}
