//
//  Button.swift
//  designsystem
//
//  Created by Rubén Alonso on 26/03/2019.
//  Copyright © 2019 Batura. All rights reserved.
//

import Foundation
import UIKit

protocol ZeroButton {
    func initStyle()
    func update()
}

extension ZeroButton {
    func initStyle() {}
    func update() {}
}

public class ActionButton: UIButton, ZeroButton {

    private var callback: (() -> Void)?

    public func completion(for event: UIControl.Event = .touchUpInside, _ completion: (() -> Void)?) {
        callback = completion
        self.addTarget(self, action: #selector(callFunction), for: event)
    }

    @objc func callFunction() {
        callback?()
    }
}

public class ZeroTextButton: ActionButton {

    public enum Mode {
        case normal
        case success
        case alert
        case error
    }

    public override var isEnabled: Bool {
        didSet {
            update()
        }
    }

    public var style: Mode = .normal {
        didSet { update() }
    }

    public init() {
        super.init(frame: CGRect.zero)
        initialize()
    }

    public override init(frame: CGRect) {
        super.init(frame: frame)
        initialize()
    }

    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initialize()
    }

    private func initialize() {
        initStyle()
        update()
    }

    func initStyle() {
        style = .normal
        titleLabel?.font = ZeroFont.bold(size: .button1)
        contentEdgeInsets = UIEdgeInsets(top: 12,
                                         left: 16,
                                         bottom: 12,
                                         right: 16)
    }

    func update() {
        if isEnabled {
            switch style {
            case .normal:
                apply(ZeroTheme.Button.normal)
            case .success:
                apply(ZeroTheme.Button.success)
            case .alert:
                apply(ZeroTheme.Button.alert)
            case .error:
                apply(ZeroTheme.Button.error)
            }
        } else {
            apply(ZeroTheme.Button.disabled)
        }
    }
}

public class ZeroContainedButton: ZeroTextButton {

    var backgroundColorEnabled: UIColor = .primary {
        didSet { update() }
    }
    var backgroundColorDisabled: UIColor = .onBackground(.low) {
        didSet { update() }
    }

    var textColorEnabled: UIColor? = .onPrimary() {
        didSet { update() }
    }
    var textColorDisabled: UIColor? = .onBackground(.medium) {
        didSet { update() }
    }

    override func initStyle() {
        super.initStyle()
        update()
        cornerRadius = 5
        shadow()
        layer.shadowOffset = CGSize(width: 0, height: 1)
    }

    override func update() {
        if isEnabled {
            backgroundColor = backgroundColorEnabled
            setTitleColor(textColorEnabled, for: .normal)
            shadow()
        } else {
            backgroundColor = backgroundColorDisabled
            setTitleColor(textColorDisabled, for: .disabled)
            shadow(opacity: 0)
        }
    }
}

public class ZeroOutlineButton: ZeroTextButton {

    var textColorEnabled: UIColor = ZeroColor.primary {
        didSet { update() }
    }
    var textColorDisabled: UIColor = .onBackground(.medium) {
        didSet { update() }
    }
    var borderColorEnabled: UIColor? {
        didSet { update() }
    }
    var borderColorDisabled: UIColor? = .onBackground(.medium) {
        didSet { update() }
    }

    override func initStyle() {
        super.initStyle()
        update()
        cornerRadius = 5
        backgroundColor = .clear
        layer.borderWidth = 1
    }

    override func update() {
        if isEnabled {
            setTitleColor(textColorEnabled, for: .normal)
            layer.borderColor = borderColorEnabled?.cgColor ?? textColorEnabled.cgColor
        } else {
            setTitleColor(textColorDisabled, for: .disabled)
            layer.borderColor = borderColorDisabled?.cgColor ?? textColorDisabled.cgColor
        }
    }
}

@IBDesignable
public class ButtonRightImage: UIControl {

    @IBInspectable
    var text: String = "Button" {
        didSet { update() }
    }

    @IBInspectable
    var image: UIImage? {
        didSet { update() }
    }

    @IBInspectable
    var textColor: UIColor = ZeroColor.onBackground() {
        didSet { update() }
    }

    @IBInspectable
    var borderColor: UIColor = ZeroColor.onBackground(.medium) {
        didSet { update() }
    }

    var font: UIFont = ZeroFont.bold(size: .button1) {
        didSet { update() }
    }

    private var imageView: UIImageView!
    private var label: UILabel!
    private var border: UIView!

    public override var tintColor: UIColor! {
        didSet { update() }
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        initView()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        initView()
    }

    private func initView() {
        imageView = UIImageView()
        label = UILabel()
        border = UILabel()

        addSubview(imageView)
        addSubview(label)
        addSubview(border)

        imageView.translatesAutoresizingMaskIntoConstraints = false
        label.translatesAutoresizingMaskIntoConstraints = false
        border.translatesAutoresizingMaskIntoConstraints = false

        NSLayoutConstraint.activate([
            label.leftAnchor.constraint(equalTo: leftAnchor, constant: 16),
            label.centerYAnchor.constraint(equalTo: centerYAnchor),
            imageView.centerYAnchor.constraint(equalTo: centerYAnchor),
            imageView.leftAnchor.constraint(equalTo: label.rightAnchor, constant: 16),
            imageView.rightAnchor.constraint(equalTo: rightAnchor, constant: -16)
        ])

        NSLayoutConstraint.activate([
            border.leftAnchor.constraint(equalTo: leftAnchor, constant: 16),
            border.rightAnchor.constraint(equalTo: rightAnchor, constant: -16),
            border.bottomAnchor.constraint(equalTo: bottomAnchor, constant: 0),
            border.heightAnchor.constraint(equalToConstant: 1)
        ])

        update()
    }

    public override var isHighlighted: Bool {
        didSet {
            label.alpha = isHighlighted ? 0.6 : 1.0
            imageView.alpha = isHighlighted ? 0.6 : 1.0
        }
    }

    private func update() {
        imageView.image = image
        imageView.tintColor = tintColor
        label.text = text
        label.font = font
        label.textColor = textColor
        border.backgroundColor = borderColor
    }
}
