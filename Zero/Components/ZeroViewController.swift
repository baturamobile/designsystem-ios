//
//  UIViewController.swift
//  designsystem
//
//  Created by Rubén Alonso on 27/03/2019.
//  Copyright © 2019 Batura. All rights reserved.
//

import Foundation
import UIKit

open class ZeroViewController: UIViewController {

    public override var title: String? {
        didSet { updateNavBar() }
    }
    public var titleFont: UIFont = ZeroFont.bold(size: .body1) {
        didSet { updateNavBar() }
    }
    public var largeTitleFont: UIFont = ZeroFont.bold(size: .head1) {
        didSet { updateNavBar() }
    }
    public var titleColor: UIColor = .onSurface() {
        didSet { updateNavBar() }
    }
    public var backImage: UIImage? = UIImage(named: "backBlack", in: Bundle(for: ZeroViewController.self), compatibleWith: nil) {
        didSet { updateNavBar() }
    }
    public var navBarBackgroundColor: UIColor? {
        didSet { updateNavBar() }
    }

    public var preferLargeTitles: Bool = false {
        didSet {
            if #available(iOS 11.0, *) {
                navigationController?.navigationBar.prefersLargeTitles = preferLargeTitles
            }
        }
    }

    public var theme: ZeroTheme.NavigationBar = .default {
        didSet { updateStyle() }
    }

    public var customView: UIView? {
        didSet { createCustomView() }
    }

    open override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = ZeroColor.background
        setupBack()
        updateStyle()
        preferLargeTitles = false
    }

    open override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }

    private func setupBack() {
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "",
                                                           style: .plain,
                                                           target: nil,
                                                           action: nil)
        navigationController?.navigationBar.backIndicatorImage = backImage
        navigationController?.navigationBar.backIndicatorTransitionMaskImage = backImage
        navigationController?.view.backgroundColor = UIColor.white
    }

    private func updateStyle() {
        switch theme {
        case .primary:
            titleColor = .onPrimary()
            navBarBackgroundColor = .primary
        default:
            titleColor = .onSurface()
            navBarBackgroundColor = .surface
        }
    }

    private func updateTitle() {
        guard let navBar = navigationController?.navigationBar else {
            return
        }
        navBar.titleTextAttributes = [.foregroundColor: titleColor,
                                      .font: titleFont]

        if #available(iOS 11.0, *) {
            navBar.largeTitleTextAttributes = [.foregroundColor: titleColor,
                                               .font: largeTitleFont]
        }
    }

    private func updateNavBar () {
        guard let navBar = navigationController?.navigationBar else {
            return
        }
        navBar.isTranslucent = false
        navBar.tintColor = titleColor
        navBar.barTintColor = navBarBackgroundColor
    }

    func createCustomView() {
        navigationItem.titleView = customView
    }
}
