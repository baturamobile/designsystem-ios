//
//  ZeroSwitch.swift
//  designsystem
//
//  Created by Rubén Alonso on 13/03/2020.
//  Copyright © 2020 Batura. All rights reserved.
//

import UIKit

public class ZeroSwitch: UISwitch {

    @IBInspectable
    var scale: CGFloat = 0.85

    public override init(frame: CGRect) {
        super.init(frame: frame)
        resize()
        setupStyle()
    }

    public required init?(coder: NSCoder) {
        super.init(coder: coder)
        resize()
        setupStyle()
    }

    func resize() {
        transform = transform.scaledBy(x: scale, y: scale)
    }

    func setupStyle() {
        onTintColor = ZeroColor.primary
    }
}
