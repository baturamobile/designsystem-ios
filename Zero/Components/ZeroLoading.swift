//
//  Loading.swift
//  designsystem
//
//  Created by Rubén Alonso on 02/07/2020.
//  Copyright © 2020 Batura. All rights reserved.
//

import UIKit

public class ZeroLoading: UIView {

    static private let loading = ZeroLoading()

    var title: String? {
        didSet { update() }
    }

    convenience private init() {
        self.init(frame: UIScreen.main.bounds)
    }

    lazy var activityOver: UIActivityIndicatorView = {
        let activity = UIActivityIndicatorView(style: .whiteLarge)
        activity.startAnimating()
        activity.transform = activity.transform.scaledBy(x: 1.2, y: 1.2)
        return activity
    }()

    lazy var labelOver: UILabel = {
        let label = UILabel()
        label.font = ZeroFont.regular(size: .body2)
        label.textColor = .background
        label.setContentCompressionResistancePriority(.required,
                                                      for: .horizontal)
        label.setContentCompressionResistancePriority(.required,
                                                      for: .vertical)
        return label
    }()

    lazy var viewOver: UIView? = {
        let view = UIView()
        view.backgroundColor = .onBackground(.medium)
        view.cornerRadius = 5
        view.translatesAutoresizingMaskIntoConstraints = false

        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.alignment = .fill
        stackView.distribution = .fill
        stackView.spacing = 16
        stackView.addArrangedSubview(self.activityOver)
        stackView.addArrangedSubview(self.labelOver)
        stackView.translatesAutoresizingMaskIntoConstraints = false

        view.addSubview(stackView)
        NSLayoutConstraint.activate([
            stackView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 24),
            stackView.topAnchor.constraint(equalTo: view.topAnchor, constant: 24),
            stackView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -24),
            stackView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -24)
        ])

        return view
    }()

    func update() {
        labelOver.text = title
        labelOver.isHidden = title?.isEmpty ?? true
    }

    func setupView() {
        backgroundColor = .clear
        let window = UIApplication.shared.windows.first ?? UIWindow()
        window.addSubview(self)
        addSubview(viewOver!)
        NSLayoutConstraint.activate([
            viewOver!.centerYAnchor.constraint(equalTo: centerYAnchor),
            viewOver!.centerXAnchor.constraint(equalTo: centerXAnchor)
        ])
    }
}

public extension ZeroLoading {
    class var title: String? {
        get { loading.title }
        set { loading.title = newValue }
    }

    class func show(text: String? = nil, hide: TimeInterval? = nil) {
        DispatchQueue.main.async {
            loading.title = text
            loading.setupView()
            loading.update()
        }
        guard let hide = hide else {
            return
        }
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + hide) {
            self.hide()
        }
    }

    class func hide() {
        loading.viewOver?.removeFromSuperview()
        loading.viewOver = nil
        loading.removeFromSuperview()
    }
}
