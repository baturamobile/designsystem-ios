//
//  Color.swift
//  designsystem
//
//  Created by Rubén Alonso on 26/03/2019.
//  Copyright © 2019 Batura. All rights reserved.
//

import Foundation
import UIKit

public typealias ZeroColor = UIColor

public extension ZeroColor {
    static var primaryColor = UIColor(red: 169.0 / 255.0, green: 0.0, blue: 128.0 / 255.0, alpha: 1.0)
    static var onPrimaryColor = UIColor.white
    static var primaryDarkColor = UIColor(red: 219.0 / 255.0, green: 165.0 / 255.0, blue: 8.0 / 255.0, alpha: 1.0)
    static var primaryLightColor = UIColor(red: 253.0 / 255.0, green: 247.0 / 255.0, blue: 230.0 / 255.0, alpha: 1.0)
    static var backgroundColor = UIColor.white
    static var onBackgroundColor = UIColor(red: 28.0 / 255.0, green: 28.0 / 255.0, blue: 30 / 255.0, alpha: 1.0)
    static var surfaceColor = UIColor(red: 250.0 / 255.0, green: 250.0 / 255.0, blue: 250.0 / 255.0, alpha: 1.0)
    static var onSurfaceColor = UIColor(red: 28.0 / 255.0, green: 28.0 / 255.0, blue: 30 / 255.0, alpha: 1.0)
    static var errorColor = UIColor(red: 195.0 / 255.0, green: 29.0 / 255.0, blue: 43.0 / 255.0, alpha: 1.0)
    static var successColor = UIColor(red: 16.0 / 255.0, green: 116.0 / 255.0, blue: 62.0 / 255.0, alpha: 1.0)
    static var alertColor = UIColor(red: 173.0 / 255.0, green: 69.0 / 255.0, blue: 0.0, alpha: 1.0)

    enum Emphasis {
        case high
        case medium
        case low
        case custom(value: CGFloat)

        var rawValue: CGFloat {
            switch self {
            case .high:
                return 1.0
            case .medium:
                return 0.74
            case .low:
                return 0.38
            case .custom(let value):
                return value
            }
        }
    }

    @nonobjc class var primary: UIColor {
        return primaryColor
    }

    static func onPrimary(_ emphasis: Emphasis = .high) -> UIColor {
        return onPrimary.withAlphaComponent(emphasis.rawValue)
    }

    private class var onPrimary: UIColor {
        return onPrimaryColor
    }

    @nonobjc class var primaryDark: UIColor {
        return primaryDarkColor
    }

    @nonobjc class var primaryLight: UIColor {
        return primaryLightColor
    }

    @nonobjc class var background: UIColor {
        return backgroundColor
    }

    static func onBackground(_ emphasis: Emphasis = .high) -> UIColor {
        return onBackground.withAlphaComponent(emphasis.rawValue)
    }

    private class var onBackground: UIColor {
        return onBackgroundColor
    }

    @nonobjc class var surface: UIColor {
        return surfaceColor
    }

    static func onSurface(_ emphasis: Emphasis = .high) -> UIColor {
        return onSurface.withAlphaComponent(emphasis.rawValue)
    }

    private class var onSurface: UIColor {
        return onSurfaceColor
    }

    @nonobjc class var error: UIColor {
        return errorColor
    }

    @nonobjc class var success: UIColor {
        return successColor
    }

    @nonobjc class var alert: UIColor {
        return alertColor
    }

    @available(iOS, deprecated: 1.1, renamed: "onBackground()")
    @nonobjc class var base500: UIColor {
        return UIColor(white: 41.0 / 255.0, alpha: 1.0)
    }

    @available(iOS, deprecated: 1.1, renamed: "onBackground(emphasis:)")
    @nonobjc class var base400: UIColor {
        return UIColor(white: 117.0 / 255.0, alpha: 1.0)
    }

    @available(iOS, deprecated: 1.1, renamed: "onBackground(emphasis:)")
    @nonobjc class var base200: UIColor {
        return UIColor(white: 189.0 / 255.0, alpha: 1.0)
    }

    @available(iOS, deprecated: 1.1, renamed: "onBackground(emphasis:)")
    @nonobjc class var base100: UIColor {
        return UIColor(white: 250.0 / 255.0, alpha: 1.0)
    }

    @available(iOS, deprecated: 1.1, renamed: "background")
    @nonobjc class var baseWhite: UIColor {
        return UIColor(white: 1.0, alpha: 1.0)
    }
}
