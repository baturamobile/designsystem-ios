//
//  Fonts.swift
//  designsystem
//
//  Created by Rubén Alonso on 01/03/2019.
//  Copyright © 2019 Batura. All rights reserved.
//

import Foundation
import UIKit

public typealias ZeroFont = UIFont

public extension ZeroFont {
    static var boldName = "CircularStd-Bold"
    static var regularName = "CircularStd-Medium"
    static var lightName = "CircularStd-Book"

    enum Size: CGFloat {
        case head1 = 34
        case head2 = 28
        case head3 = 22
        case head4 = 20
        case body1 = 17
        case body2 = 15
        case caption1 = 13
        case caption2 = 11
        static var button1: Size {
            return .body2
        }
        static var button2: Size {
            return .caption1
        }
    }

    class func bold(size: Size = .body1, scaled: Bool = true) -> UIFont {
        let font = UIFont(name: boldName, size: size.rawValue) ?? UIFont.boldSystemFont(ofSize: size.rawValue)
        return scaled ? FontMetrics.scaledFont(for: font) : font
    }
    class func regular(size: Size = .body1, scaled: Bool = true) -> UIFont {
        var font = UIFont(name: regularName, size: size.rawValue) ?? UIFont.systemFont(ofSize: size.rawValue)
        if UIAccessibility.isBoldTextEnabled {
            font = UIFont(name: boldName, size: size.rawValue) ?? UIFont.boldSystemFont(ofSize: size.rawValue)
        }
        return scaled ? FontMetrics.scaledFont(for: font) : font
    }
    class func light(size: Size = .body1, scaled: Bool = true) -> UIFont {
        var font = UIFont(name: lightName, size: size.rawValue) ?? UIFont.systemFont(ofSize: size.rawValue)
        if UIAccessibility.isBoldTextEnabled {
            font = UIFont(name: boldName, size: size.rawValue) ?? UIFont.boldSystemFont(ofSize: size.rawValue)
        }
        return scaled ? FontMetrics.scaledFont(for: font) : font
    }
}

struct FontMetrics {
    static var scaler: CGFloat {
        return UIFont.preferredFont(forTextStyle: .body).pointSize / 17.0
    }

    static func scaledFont(for font: UIFont) -> UIFont {
        if #available(iOS 11.0, *) {
            return UIFontMetrics.default.scaledFont(for: font)
        } else {
            return font.withSize(scaler * font.pointSize)
        }
    }
}
