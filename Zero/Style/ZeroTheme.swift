//
//  Theme.swift
//  designsystem
//
//  Created by Rubén Alonso on 25/02/2019.
//  Copyright © 2019 Batura. All rights reserved.
//

import Foundation
import UIKit
import MaterialComponents.MaterialChips

public struct ZeroTheme {

    public enum NavigationBar {
        case primary
        case `default`
    }

    // This apply a common style for all ui components
    public static func apply() {
        labelAppearance()
        tabBarAppearance()
        barButtonItemAppearance()
        chipViewAppearance()
        searchBarAppearance()
        tableViewAppearance()
    }

    private static func labelAppearance() {
        let labelAppearance = UILabel.appearance()
        labelAppearance.font = ZeroFont.regular(size: .body1)
        labelAppearance.textColor = ZeroColor.onBackground()
        labelAppearance.adjustsFontForContentSizeCategory = true

        let labelTableView = UILabel.appearance(whenContainedInInstancesOf: [UITableViewCell.self])
        labelTableView.font = ZeroFont.regular(size: .body1)
        labelTableView.textColor = ZeroColor.onBackground()

        let labelTabbar = UILabel.appearance(whenContainedInInstancesOf: [UITabBar.self])
        labelTabbar.font = ZeroFont.regular(size: .caption1)
        labelTabbar.textColor = ZeroColor.background
    }

    private static func tabBarAppearance() {
        let tabBarItemAppearance = UITabBarItem.appearance()
        tabBarItemAppearance.titlePositionAdjustment = UIOffset(horizontal: 0, vertical: -3)
        tabBarItemAppearance.setTitleTextAttributes([.font: ZeroFont.regular(size: .body2)], for: .normal)
        tabBarItemAppearance.setTitleTextAttributes([.font: ZeroFont.regular(size: .body2)], for: .selected)
    }

    private static func chipViewAppearance() {
        let chipViewAppearance = MDCChipView.appearance()
        chipViewAppearance.titleFont = ZeroFont.regular(size: .caption1)
        chipViewAppearance.tintColor = ZeroColor.onSurface(.low)
        chipViewAppearance.setTitleColor(ZeroColor.onSurface(), for: .normal)
        chipViewAppearance.setTitleColor(ZeroColor.primary, for: .selected)
        chipViewAppearance.setBackgroundColor(ZeroColor.onSurface().withAlphaComponent(0.12), for: .normal)
        chipViewAppearance.setBackgroundColor(ZeroColor.primary.withAlphaComponent(0.2), for: .selected)
        chipViewAppearance.selectedImageView.tintColor = ZeroColor.primary
        chipViewAppearance.mdc_adjustsFontForContentSizeCategory = true
    }

    private static func searchBarAppearance() {
        let searchBarAppearance = UISearchBar.appearance()
        searchBarAppearance.placeholderColor = ZeroColor.onBackground(.low)
        searchBarAppearance.font = ZeroFont.regular(size: .body2)
        searchBarAppearance.textColor = ZeroColor.onBackground()
        searchBarAppearance.tintColor = ZeroColor.primary
        searchBarAppearance.barTintColor = ZeroColor.primary
        searchBarAppearance.searchBarStyle = .minimal
    }

    private static func barButtonItemAppearance() {
        let barButtomItemAppearance = UIBarButtonItem.appearance()
        barButtomItemAppearance.font = ZeroFont.bold(size: .body2)
    }

    private static func tableViewAppearance() {
        let tableViewAppeareance = UITableView.appearance()
        tableViewAppeareance.separatorInset = UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 16)
        tableViewAppeareance.separatorColor = ZeroColor.onSurface(.custom(value: 0.12))
    }

    // This apply specific style to ui component
    public struct LabelFonts {
        public static let head1 = ZeroStyle<UILabel> {
            $0.font = ZeroFont.bold(size: .head1)
        }
        public static let head2 = ZeroStyle<UILabel> {
            $0.font = ZeroFont.bold(size: .head2)
        }
        public static let head3 = ZeroStyle<UILabel> {
            $0.font = ZeroFont.bold(size: .head3)
        }
        public static let head4 = ZeroStyle<UILabel> {
            $0.font = ZeroFont.bold(size: .head4)
        }
        public static let head4Regular = ZeroStyle<UILabel> {
            $0.font = ZeroFont.regular(size: .head4)
        }
        public static let body1 = ZeroStyle<UILabel> {
            $0.font = ZeroFont.bold(size: .body1)
        }
        public static let body1Regular = ZeroStyle<UILabel> {
            $0.font = ZeroFont.regular(size: .body1)
        }
        public static let body1Light = ZeroStyle<UILabel> {
            $0.font = ZeroFont.light(size: .body1)
        }
        public static let body2 = ZeroStyle<UILabel> {
            $0.font = ZeroFont.bold(size: .body2)
        }
        public static let body2Regular = ZeroStyle<UILabel> {
            $0.font = ZeroFont.regular(size: .body2)
        }
        public static let body2Light = ZeroStyle<UILabel> {
            $0.font = ZeroFont.light(size: .body2)
        }
        public static let caption1 = ZeroStyle<UILabel> {
            $0.font = ZeroFont.bold(size: .caption1)
        }
        public static let caption1Regular = ZeroStyle<UILabel> {
            $0.font = ZeroFont.regular(size: .caption1)
        }
        public static let caption1Light = ZeroStyle<UILabel> {
            $0.font = ZeroFont.light(size: .caption1)
        }
        public static let caption2 = ZeroStyle<UILabel> {
            $0.font = ZeroFont.bold(size: .caption2)
        }
        public static let caption2Regular = ZeroStyle<UILabel> {
            $0.font = ZeroFont.regular(size: .caption2)
        }
    }

    public struct Button {
        public static let contained = ZeroStyle<ZeroContainedButton> {
            $0.titleLabel?.font = ZeroFont.bold(size: .button1)

            $0.backgroundColorEnabled = ZeroColor.primary
            $0.textColorEnabled = ZeroColor.background

            $0.textColorDisabled = ZeroColor.onBackground(.low)
            $0.backgroundColorDisabled = ZeroColor.onBackground(.medium)
        }

        public static let outlined = ZeroStyle<ZeroOutlineButton> {
            $0.titleLabel?.font = ZeroFont.regular(size: .button1)

            $0.textColorEnabled = ZeroColor.primary

            $0.textColorDisabled = ZeroColor.onBackground(.low)
            $0.borderColorDisabled = ZeroColor.black.withAlphaComponent(0.12)
        }

        public static let normal = ZeroStyle<ZeroTextButton> {
            $0.titleLabel?.font = ZeroFont.regular(size: .button1)
            $0.setTitleColor(ZeroColor.primary, for: .normal)
        }

        public static let error = ZeroStyle<ZeroTextButton> {
            $0.titleLabel?.font = ZeroFont.regular(size: .button1)
            $0.setTitleColor(ZeroColor.error, for: .normal)
        }

        public static let success = ZeroStyle<ZeroTextButton> {
            $0.titleLabel?.font = ZeroFont.regular(size: .button1)
            $0.setTitleColor(ZeroColor.success, for: .normal)
        }

        public static let disabled = ZeroStyle<ZeroTextButton> {
            $0.titleLabel?.font = ZeroFont.regular(size: .button1)
            $0.setTitleColor(ZeroColor.onBackground(.medium), for: .normal)
        }

        public static let alert = ZeroStyle<ZeroTextButton> {
            $0.titleLabel?.font = ZeroFont.regular(size: .button1)
            $0.setTitleColor(ZeroColor.alert, for: .normal)
        }

        public static let bottomSheet = ZeroStyle<ZeroTextButton> {
            $0.titleLabel?.font = ZeroFont.regular(size: .button1)
            $0.setTitleColor(ZeroColor.onSurface(), for: .normal)
            $0.tintColor = ZeroColor.onSurface()
            $0.contentHorizontalAlignment = .left
            $0.titleEdgeInsets = UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 0)
        }
    }

    public struct SearchBarStyle {
        public static let searchBar = ZeroStyle<UISearchBar> {
            $0.placeholderColor = ZeroColor.onBackground(.low)
            $0.font = ZeroFont.regular(size: .body2)
            $0.textColor = ZeroColor.onBackground()

            $0.tintColor = ZeroColor.primary
            $0.barTintColor = ZeroColor.primary
            $0.searchBarStyle = .minimal
        }
    }

    public struct SnackbarStyle {
        public static let normal = ZeroStyle<ZeroSnackbar> {
            $0.textColor = ZeroColor.background
            $0.buttonColor = ZeroColor.primary
            $0.textFont = ZeroFont.regular(size: .body2)
            $0.buttonFont = ZeroFont.bold(size: .button2)
        }
    }
}
