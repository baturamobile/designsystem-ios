//
//  Style.swift
//  designsystem
//
//  Created by Rubén Alonso on 01/03/2019.
//  Copyright © 2019 Batura. All rights reserved.
//

import Foundation
import UIKit

public struct ZeroStyle<View: Any> {

    public let style: (View) -> Void

    public init(_ style: @escaping (View) -> Void) {
        self.style = style
    }

    public func apply(to view: View) {
        style(view)
    }

    public func apply(to views: [View]) {
        for view in views {
            style(view)
        }
    }
}

extension UIView {
    public convenience init<V>(style: ZeroStyle<V>) {
        self.init(frame: .zero)
        apply(style)
    }

    public func apply<V>(_ style: ZeroStyle<V>) {
        guard let view = self as? V else {
            print("💥 Could not apply style for \(V.self) to \(type(of: self))")
            return
        }
        style.apply(to: view)
    }

    public func apply<V>(_ styles: [ZeroStyle<V>]) {
        guard let view = self as? V else {
            print("💥 Could not apply style for \(V.self) to \(type(of: self))")
            return
        }
        for style in styles {
            style.apply(to: view)
        }
    }
}
