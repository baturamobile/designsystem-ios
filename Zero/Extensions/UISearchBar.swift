//
//  SearcBar.swift
//  designsystem
//
//  Created by Rubén Alonso on 17/04/2019.
//  Copyright © 2019 Batura. All rights reserved.
//

import UIKit

public extension UISearchBar {
    enum Properties {
        case color
        case icon
        case font
        case textColor
        case cancelColor
        case placeholderColor
    }

    private static var properties = [Properties: Any]()

    override open var backgroundColor: UIColor? {
        didSet { update() }
    }

    var searchIcon: UIImage? {
        get {
            return UISearchBar.properties[.icon] as? UIImage
        }
        set {
            UISearchBar.properties[.icon] = newValue
            update()
        }
    }
    var font: UIFont? {
        get {
            return UISearchBar.properties[.font] as? UIFont
        }
        set {
            UISearchBar.properties[.font] = newValue
            update()
        }
    }

    var textColor: UIColor? {
        get {
            return UISearchBar.properties[.textColor] as? UIColor
        }
        set {
            UISearchBar.properties[.textColor] = newValue
            update()
        }
    }
    var placeholderColor: UIColor? {
        get {
            return UISearchBar.properties[.placeholderColor] as? UIColor
        }
        set {
            UISearchBar.properties[.placeholderColor] = newValue
            update()
        }
    }

    var cancelColor: UIColor? {
        get {
            return UISearchBar.properties[.cancelColor] as? UIColor
        }
        set {
            UISearchBar.properties[.cancelColor] = newValue
            update()
        }
    }

    func update() {
        if #available(iOS 13, *) {
            searchTextField.textColor = textColor
            searchTextField.font = font
            searchTextField.backgroundColor = backgroundColor
            searchTextField.leftView = searchIcon != nil ? UIImageView(image: searchIcon) : searchTextField.leftView
            if let placeholderColor = placeholderColor {
                let attributes = [NSAttributedString.Key.foregroundColor: placeholderColor]
                let string = searchTextField.attributedPlaceholder?.string
                searchTextField.attributedPlaceholder = NSAttributedString(string: string ?? "",
                                                                           attributes: attributes)
            }
        } else {
            let searchField = self.value(forKey: "_searchField") as? UITextField ?? UITextField()
            searchField.textColor = textColor
            searchField.font = font
            searchField.backgroundColor = backgroundColor
            searchField.leftView = searchIcon != nil ? UIImageView(image: searchIcon) : searchField.leftView

            if let placeholderColor = placeholderColor {
                let attributes = [NSAttributedString.Key.foregroundColor: placeholderColor]
                let string = searchField.attributedPlaceholder?.string
                searchField.attributedPlaceholder = NSAttributedString(string: string ?? "",
                                                                       attributes: attributes)
            }
            self.setValue(searchField, forKey: "searchField")
        }

        if let buttonItem = self.subviews.first?.subviews.last as? UIButton {
            buttonItem.titleLabel?.font = ZeroFont.regular(size: .body2)
            if let cancelColor = cancelColor {
                buttonItem.setTitleColor(cancelColor, for: .normal)
            }
        }
    }
}
