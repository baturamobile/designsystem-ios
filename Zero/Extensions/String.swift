//
//  String.swift
//  designsystem
//
//  Created by Rubén Alonso on 12/11/2019.
//  Copyright © 2019 Batura. All rights reserved.
//

import Foundation

public extension String {
    private subscript (idx: Int) -> Character {
        return self[index(startIndex, offsetBy: idx)]
    }

    subscript (idx: Int) -> String {
        return String(self[idx] as Character)
    }

    subscript (range: CountableClosedRange<Int>) -> String {
        let start = index(startIndex, offsetBy: range.lowerBound)
        let end = index(startIndex, offsetBy: range.upperBound)
        return String(self[start ... end])
    }

    enum ValidateType: String {
        case email = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$"
        case nif = "^[0-9]{8}[TRWAGMYFPDXBNJZSQVHLCKET]{1}$"
        case nie = "^[XYZ]{1}[0-9]{7}[TRWAGMYFPDXBNJZSQVHLCKET]{1}$"
        case phone = "^[0-9]{2,3}-? ?[0-9]{6,7}$"
    }

    func validate(type: ValidateType,
                  options: NSRegularExpression.Options = [.anchorsMatchLines,
                                                          .caseInsensitive]) -> Bool {
        let pattern = type.rawValue
        do {
            let regex = try NSRegularExpression(pattern: pattern, options: options)
            let matches = regex.matches(in: self, options: .anchored, range: NSRange(location: 0,
                                                                                     length: self.count))
            guard matches.count > 0 else {
                return false
            }
            if type == .nie || type == .nif {
                return validateNif()
            }
            return true

        } catch {
            return false
        }
    }

    func validate(types: [ValidateType]) -> (result: Bool, types: [ValidateType]) {
        var result = (result: true, types:[ValidateType]())
        for type in types {
            if !validate(type: type) {
                result.result = false
                result.types.append(type)
            }
        }
        return result
    }

    private func validateNif() -> Bool {
        let validChars = "TRWAGMYFPDXBNJZSQVHLCKET"
        let range = startIndex..<index(startIndex, offsetBy: 1)
        let textReplaced = self
            .uppercased()
            .replacingOccurrences(of: "X", with: "0", range: range)
            .replacingOccurrences(of: "Y", with: "1", range: range)
            .replacingOccurrences(of: "Z", with: "2", range: range)

        let letter = uppercased().last
        let charIndex = Int(textReplaced[0...7])! % 23
        return (validChars[charIndex] == letter)
    }
}
