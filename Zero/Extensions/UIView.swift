//
//  UIView.swift
//  designsystem
//
//  Created by Rubén Alonso on 13/03/2019.
//  Copyright © 2019 Batura. All rights reserved.
//

import Foundation
import UIKit

public extension UIView {
    enum Border {
        case top, left, right, bottom
    }

    var cornerRadius: CGFloat {
        get { return layer.cornerRadius }
        set { layer.cornerRadius = newValue }
    }

    func cornerRadius( _ size: CGSize, corners: UIRectCorner = .allCorners) {
        let bezier = UIBezierPath(roundedRect: self.bounds,
                                  byRoundingCorners: corners,
                                  cornerRadii: size)

        let layer = CAShapeLayer()
        layer.path = bezier.cgPath

        self.layer.mask = layer
    }

    func cornerRadius(_ size: CGFloat, corners: UIRectCorner = .allCorners) {
        let radius = CGSize(width: size, height: size)
        cornerRadius(radius, corners: corners)
    }

    func shadow(opacity: Float = 0.3,
                color: UIColor = .black,
                offset: CGSize = CGSize(width: 0, height: 1),
                radius: CGFloat = 3) {
        layer.shadowOpacity = opacity
        layer.shadowColor = color.cgColor
        layer.shadowOffset = offset
        layer.shadowRadius = radius
    }

    func removeAllSubviews() {
        subviews.forEach({$0.removeFromSuperview()})
    }

    func setAnchorPoint(_ point: CGPoint) {
        var newPoint = CGPoint(x: bounds.size.width * point.x, y: bounds.size.height * point.y)
        var oldPoint = CGPoint(x: bounds.size.width * layer.anchorPoint.x, y: bounds.size.height * layer.anchorPoint.y)

        newPoint = newPoint.applying(transform)
        oldPoint = oldPoint.applying(transform)

        var position = layer.position

        position.x -= oldPoint.x
        position.x += newPoint.x

        position.y -= oldPoint.y
        position.y += newPoint.y

        layer.position = position
        layer.anchorPoint = point
    }

    func addBorder(borders: Set<Border>, color: UIColor, thickness: CGFloat = 1) {
        var frame: CGRect = .zero
        var autoresizingMask: AutoresizingMask = []
        for border in borders {
            switch border {
            case .top:
                frame = CGRect(origin: CGPoint(x: self.bounds.minX,
                                               y: self.bounds.minY),
                               size: CGSize(width: self.bounds.width,
                                            height: thickness))
                autoresizingMask = [.flexibleBottomMargin, .flexibleWidth]

            case .left:
                frame = CGRect(origin: CGPoint(x: self.bounds.minX,
                                               y: self.bounds.minY),
                               size: CGSize(width: thickness,
                                            height: self.bounds.height))

                autoresizingMask = [.flexibleHeight, .flexibleRightMargin]
            case .right:
                frame = CGRect(origin: CGPoint(x: self.bounds.maxX,
                                               y: self.bounds.minY),
                               size: CGSize(width: thickness,
                                            height: self.bounds.height))
                autoresizingMask = [.flexibleHeight, .flexibleLeftMargin]
            case .bottom:
                frame = CGRect(origin: CGPoint(x: self.bounds.minX,
                                               y: self.bounds.maxY - thickness),
                               size: CGSize(width: self.bounds.width,
                                            height: thickness))
                autoresizingMask = [.flexibleTopMargin, .flexibleWidth]
            }
            let view = UIView()
            view.frame = frame
            view.backgroundColor = color
            view.autoresizingMask = autoresizingMask
            addSubview(view)
        }
    }
}
