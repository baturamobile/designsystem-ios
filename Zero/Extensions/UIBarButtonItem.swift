//
//  UIBarButtonItem.swift
//  designsystem
//
//  Created by Rubén Alonso on 23/04/2019.
//  Copyright © 2019 Batura. All rights reserved.
//

import Foundation
import UIKit

public extension UIBarButtonItem {
    var font: UIFont? {
        get {
            return titleTextAttributes(for: .normal)?[.font] as? UIFont
        }
        set {
            setTitleTextAttributes([.font: newValue as Any], for: .normal)
            setTitleTextAttributes([.font: newValue as Any], for: .highlighted)
        }
    }
}
