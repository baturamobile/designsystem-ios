
Pod::Spec.new do |spec|

  spec.name         = "Zero"
  spec.version      = "1.2.0"
  spec.summary      = "Zero DS from Batura."
  spec.description  = <<-DESC
  Zero DS from Batura
                   DESC

  spec.homepage     = "https://batura.atlassian.net/wiki/spaces/BDS/overview"
  spec.license      = "MIT"
  spec.author       = { "ruben" => "ruben@baturamobile.com" }
  spec.source       = { :git => 'https://bitbucket.org/baturamobile/designsystem-ios.git', :tag => "#{spec.version}" }
  spec.source_files  = "Zero", "Zero/**/*.{h,m,swift,xib}"
  spec.resources = "Zero/Resources/*"

  spec.ios.deployment_target = '10.0'
  spec.swift_versions = ['5.1', '5.2', '5.3']

  spec.dependency "MaterialComponents/TextFields"
  spec.dependency "MaterialComponents/Chips"

end
